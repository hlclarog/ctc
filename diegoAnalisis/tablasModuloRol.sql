-- MySQL Workbench Synchronization
-- Generated: 2016-12-28 08:48
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: diego.reyes

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;

CREATE TABLE IF NOT EXISTS `ctc`.`rol` (
  `idRol` INT(11) NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(45) NOT NULL,
  `Estado` VARCHAR(10) NOT NULL DEFAULT 'Activo',
  PRIMARY KEY (`idRol`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE TABLE IF NOT EXISTS `ctc`.`modulo` (
  `idModulo` INT(11) NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(45) NOT NULL,
  `Estado` VARCHAR(10) NOT NULL DEFAULT 'Activo',
  PRIMARY KEY (`idModulo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE TABLE IF NOT EXISTS `ctc`.`usuario_rol` (
  `idUsuario` INT(11) NOT NULL,
  `idRol` INT(11) NOT NULL,
  PRIMARY KEY (`idUsuario`, `idRol`),
  INDEX `fk_usuario_has_rol_rol1_idx` (`idRol` ASC),
  INDEX `fk_usuario_has_rol_usuario_idx` (`idUsuario` ASC),
  CONSTRAINT `fk_usuario_has_rol_usuario`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `ctc`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_has_rol_rol1`
    FOREIGN KEY (`idRol`)
    REFERENCES `ctc`.`rol` (`idRol`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `ctc`.`modulo_rol` (
  `idModulo` INT(11) NOT NULL,
  `idRol` INT(11) NOT NULL,
  PRIMARY KEY (`idModulo`, `idRol`),
  INDEX `fk_modulo_has_rol_rol1_idx` (`idRol` ASC),
  INDEX `fk_modulo_has_rol_modulo1_idx` (`idModulo` ASC),
  CONSTRAINT `fk_modulo_has_rol_modulo1`
    FOREIGN KEY (`idModulo`)
    REFERENCES `ctc`.`modulo` (`idModulo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_modulo_has_rol_rol1`
    FOREIGN KEY (`idRol`)
    REFERENCES `ctc`.`rol` (`idRol`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE TABLE IF NOT EXISTS `ctc`.`region` (
  `idRegion` INT(11) NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(45) NOT NULL,
  `Estado` VARCHAR(10) NOT NULL DEFAULT 'Activo',
  PRIMARY KEY (`idRegion`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;
  

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
