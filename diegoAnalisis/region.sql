-- MySQL Workbench Synchronization
-- Generated: 2016-12-28 14:13
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: diego.reyes

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;

ALTER TABLE `ctc`.`departamento` 
ADD COLUMN `idRegion` INT(11) NOT NULL AFTER `nombre`,
ADD INDEX `fk_departamento_region_idx` (`idRegion` ASC);
 

ALTER TABLE `ctc`.`departamento` 
ADD CONSTRAINT `fk_departamento_region`
  FOREIGN KEY (`idRegion`)
  REFERENCES `ctc`.`region` (`idRegion`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

 
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;