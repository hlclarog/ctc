DROP PROCEDURE IF EXISTS  usuario_rol_seleccionar;
DROP PROCEDURE IF EXISTS modulo_adicionar;
DROP PROCEDURE IF EXISTS modulo_rol_adicionar;
DROP PROCEDURE IF EXISTS modulo_rol_seleccionar;
DROP PROCEDURE IF EXISTS modulo_seleccionar;
DROP PROCEDURE IF EXISTS rol_adicionar;
DROP PROCEDURE IF EXISTS rol_seleccionar;
DROP PROCEDURE IF EXISTS region_seleccionar;
DROP PROCEDURE IF EXISTS region_adicionar;
DROP PROCEDURE IF EXISTS usuario_rol_adicionar;


CREATE PROCEDURE `usuario_rol_seleccionar`(IN `id` INT) NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER SELECT u.nombre_apellido , r.idRol , u.id_usuario, r.Nombre AS NombreRol FROM usuario_rol ur INNER JOIN usuario u ON u.id_usuario = ur.idUsuario INNER JOIN rol r ON ur.idRol = r.idRol WHERE u.id_usuario = id;
CREATE PROCEDURE `modulo_adicionar`(IN `nombreM` VARCHAR(100)) NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER INSERT INTO modulo( Nombre) VALUES ( nombreM );
CREATE PROCEDURE `modulo_rol_adicionar`(IN `idModuloM` INT, IN `idRolR` INT) NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER INSERT INTO modulo_rol (idModulo , idRol) VALUES (idModuloM , idRolR);
CREATE PROCEDURE `modulo_rol_seleccionar`(IN `idRolRM` INT) NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER SELECT mr.idRol , mr.idModulo , r.Nombre AS NombreRol , m.Nombre AS NombreModulo FROM modulo_rol mr INNER JOIN modulo m ON m.idModulo = mr.idModulo INNER JOIN rol r ON r.idRol = mr.idRol WHERE mr.idRol = idRolRM;
CREATE PROCEDURE `modulo_seleccionar`() NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER SELECT idModulo , Nombre , Estado FROM modulo WHERE estado = 'Activo';
CREATE PROCEDURE `rol_adicionar`(IN `nombreR` VARCHAR(100)) NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER INSERT INTO rol( Nombre ) VALUES ( nombreR );
CREATE PROCEDURE `rol_seleccionar`() NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER SELECT r.idRol , r.Nombre , r.Estado FROM rol r WHERE r.Estado = 'Activo';
CREATE PROCEDURE `region_seleccionar`() NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER SELECT r.idRegion , r.Nombre , r.Estado FROM region r WHERE r.Estado = 'Activo';
CREATE PROCEDURE `region_adicionar`(IN `nombreR` VARCHAR(100)) NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER INSERT INTO region( Nombre ) VALUES ( nombreR );
CREATE PROCEDURE `rol_adicionar`(IN `idRol` INT , IN `idUsuario`) NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER INSERT INTO usuario_rol( idRol, idUsuario ) VALUES ( idRol , idUsuario );

 
