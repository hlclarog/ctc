<?php 
/* * Vista Federaciones * Excellentiam S.E. * Fecha creacion: 17/09/14 */
include($_SERVER['DOCUMENT_ROOT']."/application/views/funcionesGenericas.php");
$tituloPagina = "Gesti&oacute;n Directivos";
$soloLectura = "";
if(isset($error))  $tab = 'tabAdicionar';
else  
	$tab = 'tabConsultar';
if(isset($consultar)){  
	if($consultar == "1")      
		$tituloTab = 'Consulta';  
else      
	$tituloTab = 'Modificar';  
$tab = 'tabModificar';  
$tabActivo = 1;  
if($consultar == "1")      
	$soloLectura = "readonly";}else{  $tituloTab = 'Modificar';  
$tabActivo = 2;}
//CabeceraSinMenu($tituloPagina, $usuario, $tab, $tabActivo);
if(!isset($consultar) || $tituloTab == 'Consulta')
	{  
	echo form_open('controladorDirectivoFederacion/AdicionarDirectivoFederacion');  
	echo "<script src='/js/directivoFederacionAdicionar.js'></script>";}
	else
	{  
	echo form_open('controladorDirectivoFederacion/ModificarDirectivoFederacion');  
	echo "<script src='/js/directivoFederacionModificar.js'></script>";}
	/** Mensajes de eliminacion de registros*/
	if(isset($estadoEliminar) && $estadoEliminar == true)  
	echo '<div id="dialogo" align="center" class="ventana" title="Informacion">          Se elimino satisfactoriamente el registro de directivo.        </div>';
else 
	if(isset($estadoEliminar) && $estadoEliminar == false)  
		echo '<div id="dialogo" align="center" class="ventana" title="Informacion">          Ocurrio un problema al eliminar el registro de directivo.        </div>';
	/** Mensajes de modificacion de registros*/
	if(isset($estadoModificar) && $estadoModificar == true)  
		echo '<div id="dialogo" align="center" class="ventana" title="Informacion">          Se actualizo el registro de directivo satisfactoriamente.        </div>';
	else 
		if(isset($estadoModificar) && $estadoModificar == false)  
			echo '<div id="dialogo" align="center" class="ventana" title="Informacion">          Error al actualizar el registro de directivo.        </div>';
		/** Mensajes de adicion de registros*/
	if(isset($estadoAdicionar) && $estadoAdicionar == true)  
		echo '<div id="dialogo" align="center" class="ventana" title="Informacion">          Se adicion&oacute el registro de directivo satisfactoriamente.        </div>';
	else if(isset($estadoAdicionar) && $estadoAdicionar == false)  
		echo '<div id="dialogo" align="center" class="ventana" title="Informacion">          Error al adicionar el registro de directivo.        </div>';
	/** Validacion de modo consulta detallada*/
	if(isset($consultar) && $consultar == "1")  
	echo "<script src='/js/directivo.js'></script>";
	if($usuario['perfil'] == 'Lector Federacion')    
		echo "<script type='text/javascript'>            $(function() {                 $('#tabs').tabs('disable', 1);            });          
	</script>";?><div id='divTituloPrincipal'>  <?php echo $tituloPagina; ?></div><div id="tabs1" class="divTabs">  
	<ul>      
	<li><a href="#tabAdicionar">Adicionar</a></li>      
	</ul>  
	      
 <div id="tabAdicionar">      
<div id="divEmpresa" class="clearfix">          
<ul id="sidemenu">              
<li>                  
<a href="#informacion-content" class="open"><i class="icon-home icon-large"></i>Informaci&oacute;n Directivo Federacion</a>              
</li>          
</ul>          
<div id="content">              
<!--Tab Datos Basicos-->              
<div id="informacion-content" class="contentblock">                  
<div align="center">                      
<fieldset align="center">                          
<legend align="left" class="legend">Datos B&aacute;sicos</legend>                                
<table align="left" style="margin-top: 15px;">                                                    
<tr>                                                             
<td align="left" class="tdFormulario">                                                  
<label>Nombre  y Apellido</label>                                                
</td>                                                                   
<td align="left" class="tdFormulario">                                             
<div class="divControl">                                                                                             
<input  type="text" name="txtNombreApellidoAdic" id="txtNombreApellidoAdic" value="" />                                              
</div>                                                                                                      
<div class="campoObligatorio">*</div>                                                                                                            
</td>                                                      
</tr>                                        
<tr>                                        
<td align="left" class="tdMiddle">                                            
<label>C&eacute;dula</label>                                        
</td>                                        
<td align="left" class="tdFormulario">                                            
<div>                                                
<div class="divControl">                                                    
<input type="text" id="txtCedulaDirectivoAdic" name="txtCedulaDirectivoAdic" />                                                                                              
</div>                                                
<div class="campoObligatorio">*</div>                                                
<div class="clearBoth"></div>                                                
<div id="divCedulaVal" class="fuenteRoja"></div>                                            
</div>                                        
</td>                                    
</tr>                                                                        
<tr>                                                                    
<td align="left" class="tdFormulario">                                               
<label>Fecha de Nacimiento</label>                                                       
</td>                                                           
<td align="left" class="tdFormulario">                                                         
<div class="divControl">                                            
<input type="text" id="datepicker10" name="txtFechaNacimientoAdic" id="txtFechaNacimientoAdic" value="" />                                                    
</div>                                        
<div class="campoObligatorio">*</div>                                            
</td>                                                           
</tr>                                                      
<tr>                                                             
<td align="left" class="tdFormulario">                                                 
<label>Edad por Categor&iacute;as</label>                                                        
</td>                                                                 
<td align="left" class="tdFormulario">                                                 
<div class="divControl">                                            
<select name="sltEdadCategoriasAdic" id="sltEdadCategoriasAdic">                                                    
<option value=""> Seleccionar </option>                                                           
<?php                                                                                 
LlenarSelectOption($edadPorCategorias)                                                
?>                                            
</select>                                                                     
</div>                                                                                                      
<div class="campoObligatorio">*</div>                                        
</td>                                                       
</tr>                                                    
<tr>                                                              
<td align="left" class="tdFormulario">                                               
<label>N&uacute;mero de Celular</label>                                            
</td>                                                          
<td align="left" class="tdFormulario">                                                 
<input type="text" name="txtNumeroCelularAdic" id="txtNumeroCelularAdic" value="" />                                          
</td>                                        
</tr>                                      
<tr>                                                              
<td align="left" class="tdFormulario">                                               
<label>Tel&eacute;fono</label>                                            
</td>                                                          
<td align="left" class="tdFormulario">                                                 
<input type="text" name="txtTelefonosAdic" id="txtTelefonosAdic" value="" />                                          
</td>                                        
</tr>                                     
<tr>                                                      
<td align="left" class="tdFormulario">                                                   
<label>Correo Electr&oacute;nico</label>                                          
</td>                                                        
<td align="left" class="tdFormulario">                                                             
<input type="text" name="txtCorreoAdic" id="txtCorreoAdic" value="" />                                            
</td>                                                     
</tr>                                                        
<tr>                                                              
<td align="left" class="tdFormulario">                                                 
<label>Usuario Facebook</label>                                                
</td>                                                            
<td align="left" class="tdFormulario">                                                     
<input type="text" name="txtUsuarioFacebookAdic" id="txtUsuarioFacebookAdic" value="" />                                        
</td>                                              
</tr>                                                          
<tr>                                                            
<td align="left" class="tdFormulario">                                               
<label>Usuario Twiter</label>                                                
</td>                                                                 
<td align="left" class="tdFormulario">                                                   
<input type="text" name="txtUsuarioTwiterAdic" id="txtUsuarioTwiterAdic" value="" />                                                  
</td>                                             
</tr>                                                              
<tr>                                                           
<td align="left" class="tdFormulario">                                            
<label>Nivel educativo</label>                                                              
</td>                                        
<td align="left" class="tdFormulario">                                                  
<div class="divControl">                                                                                                                                    
<select name="sltNivelEducativoAdic" id="sltNivelEducativoAdic">                                                
<option value=""> Seleccionar </option>                                                        
<?php                                                                                 
LlenarSelectOption($nivelEducativo)                                                
?>                                            
</select>                                                           
</div>                                                                                                       
<div class="campoObligatorio">*</div>                                                                                    
</td>                                                    
</tr>                                                  
<tr>                                                            
<td align="left" class="tdFormulario">                                                
<label>Cargo</label>                                                  
</td>                                                          
<td align="left" class="tdFormulario">                                                       
<div class="divControl">                                                                                                                                    
<select name="sltCargoDirectivoAdic" id="sltCargoDirectivoAdic">                                                 
<option value="">Seleccionar </option>                                                     
<?php                                                                                 
LlenarSelectOption($cargos)                                                
?>                                            
</select>                                                   
</div>                                                                                                      
<div class="campoObligatorio">*</div>                                                                                    
</td>                                                      
</tr>                                                          
<tr>                                                            
<td align="left" class="tdFormulario">                                                
<label>Federacion al que Pertenece</label>                                                  
</td>                                                          
<td align="left" class="tdFormulario">                                                       
<div class="divControl">                                                                                                                                    
<select name="sltFederacionDirectivoAdic" id="sltFederacionDirectivoAdic">                                                 
<option value="">Seleccionar </option>                                                     
<?php                                                                                 
LlenarSelectOption($federacionDirectivo)                                                
?>                                            
</select>                                                   
</div>                                                                                                      
<div class="campoObligatorio">*</div>                                                                                    
</td>                                                      
</tr>                                                                                          
</table>                      
</fieldset>                  
</div>              
</div>          
</div>      
</div>      
<div id="divError"></div>      
<div class="clearBoth"></div>      
<div class="divGuardar">          
<button type="submit" class="submit">              
<img src="/images/guardar.jpg" width="36" height="36" />              
<br/>              Guardar          </button>      
</div>  
</div>  
 </div>