<?php
/*
 * * Vista Usuarios * Excellentiam S.E. *
 * Fecha creacion: 17/09/14
 */
include($_SERVER['DOCUMENT_ROOT']."/application/views/funcionesGenericas.php");

if(isset($registros[0]['nombre_apellido'] ) )
$tituloPagina = "Usuario ".$registros[0]['nombre_apellido'];
else
$tituloPagina = "Usuario ";

$soloLectura = "";
if(isset($error))
    $tab = 'tabAdicionar';
else
    $tab = 'tabConsultar';

if(isset($consultar))
{  if($consultar == "1")
    $tituloTab = 'Consulta';
else
    $tituloTab = 'Modificar';

    $tab = 'tabModificar';
    $tabActivo = 1;
    if($consultar == "1")
        $soloLectura = "readonly";

}
else
{
    $tituloTab = 'Modificar';
    $tabActivo = 2;

}

Cabecera($tituloPagina, $usuario, $tab, $tabActivo);

if(!isset($consultar) || $tituloTab == 'Consulta')
{
    echo form_open('controladorUsuarioRol/AdicionarUsuarioRol');
    echo "<script src='/js/usuarioRolAdicionar.js'></script>";

}
else
{  echo form_open('controladorUsuarioRol/ModificarUsuarioRol');
    echo "<script src='/js/moduloRolModificar.js'></script>";

}
/** Mensajes de eliminacion de registros*/
if(isset($estadoEliminar) && $estadoEliminar == true)
    echo '<div id="dialogo" align="center" class="ventana" title="Informacion">          Se elimino satisfactoriamente el registro de rol al usuario.        </div>';
else if(isset($estadoEliminar) && $estadoEliminar == false)
    echo '<div id="dialogo" align="center" class="ventana" title="Informacion">          Ocurrio un problema al eliminar el registro de ROL AL Usuario.        </div>';
/** Mensajes de modificacion de registros*/
if(isset($estadoModificar) && $estadoModificar == true)
    echo '<div id="dialogo" align="center" class="ventana" title="Informacion">          Se actualizo el registro de rol satisfactoriamente.        </div>';
else if(isset($estadoModificar) && $estadoModificar == false)
    echo '<div id="dialogo" align="center" class="ventana" title="Informacion">          Error al actualizar el registro de rol al usuario.        </div>';
/** Mensajes de adicion de registros*/
if(isset($estadoAdicionar) && $estadoAdicionar == true)
    echo '<div id="dialogo" align="center" class="ventana" title="Informacion">          Se adicion&oacute; el registro de rol satisfactoriamente al usuario.        </div>';
else if(isset($estadoAdicionar) && $estadoAdicionar == false)
    echo '<div id="dialogo" align="center" class="ventana" title="Informacion">          Error al adicionar el registro de rol al usuario.        </div>';
/** Validacion de modo consulta detallada*/
if(isset($consultar) && $consultar == "1")
    echo "<script src='/js/modulo.js'></script>";
?><div id='divTituloPrincipal'>
<?php echo $tituloPagina; ?>
    </div><div id="tabs" class="divTabs">
    <ul>
        <li><a href="#tabConsultar" class="limpiarFormulario">Consultar</a></li>
        <li><a href="#tabAdicionar">Adicionar</a></li>
        <li><a href="#tabModificar">
                <?php echo $tituloTab ?>
            </a></li>
    </ul>    <div id="tabConsultar" align="center" style="padding-top: 25px;">
        <table id="table" class="display" cellspacing="0" width="100%">
            <thead class="trTitulo">
            <tr>
                <th></th>
                <th>idRol</th>
                <th>Usuario</th>
                <th>Rol</th>
                <td></td>
            </tr>
            </thead>
            <tbody>
            <?php
            /*
             * * $registros: Array en donde se obtienen los resultados del
             * * $registro: Donde se almacenaran el registro actual para graficar
             */
            if($tituloTab != 'Consulta')
            {
                if(!isset($consultar))
                {
                    foreach($registros as $registro)
                    {
                        echo "<tr>";
                        if($usuario['perfil'] == "Lector")
                            echo '<td>
                                                <a href="/index.php/controladorModuloRol/ConsultarModuloRol/'.$registro['id_usuario'].'/1" title="Consultar">
                                                    <img src="/images/buscar.png" width="15" height="15" alt="Consultar"/>
                                                </a>
                                            </td>';
                        else
                        {
                            echo '<td>
                                                <a href="/index.php/controladorModulo/ConsultarModuloRol/'.$registro['id_usuario'].'" title="Modificar">
                                                    <img src="/images/editar.jpg" width="15" height="15"  alt="Editar"/>
                                                </a>                          
                                                <br>
                                                <a href="javascript:;" onclick="Confirmar(\'/index.php/controladorModuloRol/EliminarModuloRol/'.$registro['id_usuario'].'\'); return false;"  title="Eliminar">
                                                    <img src="/images/eliminar.png" width="15" height="15" alt="Eliminar"/>
                                                </a>
                                            </td>';
                        }
                        echo "<td>".utf8_decode($registro['idRol'])."</td>";
                        echo "<td>".utf8_decode($registro['nombre_apellido'])."</th>";
                        echo "<td>".utf8_decode($registro['NombreRol'])."</td>";
                        echo "<td></td>";
                        echo "</tr>";
                    }

                }
                else
                    $registro = $registros[0];
            }
            else
                $registro = $registros[0];
            ?>

            </tbody>
        </table>
        <div>
            <div id="divExportarExcel">
                <a href="/index.php/controladorModuloRol/GenerarExcel" target="_blank" title="Exportar a formato Excel">
                    <img src="/images/excel.jpg" width="30" height="30" />
                    <br />
                    Exportar a Excel
                </a>
            </div>
            <div id="divExportarPdf">
                <a href="/index.php/controladorModuloRol/GenerarPdf" target="_blank" title="Exportar a formato PDF">
                    <img src="/images/pdf.jpg" width="30" height="30" />
                    <br />
                    Exportar a Pdf
                </a>
            </div>
            <div class="clearBoth"></div>
        </div>
    </div>
    <div id="tabAdicionar">        
    <div id="divConvenio" class="clearfix">            
    <ul id="sidemenu">                
    <li>                    
    <a href="#informacion-content" class="open"><i class="icon-home icon-large"></i>Informaci&oacute;n B&aacute;sica</a>                
    </li>            
    </ul>            
    <div id="content">                
    <!--Tab Datos Basicos-->                
    <div id="informacion-content" class="contentblock">                    
    <div align="center">                        
    <fieldset align="center">                          
    <legend align="left" class="legend">Datos Rol</legend>                            
    <table align="left">                                                               
     <tr>                                   
      <td align="left" class="tdFormulario">                                        
      <label>Rol </label>                                    
      </td>                                    
      <td align="left" class="tdFormulario">                                        
      <div class="divControl"> 
      <input type="hidden" name="txtIdUsuario" id="txtIdUsuario" value="<?=$idUsuario?>"  >                                           
      <select name="ddlRolAdic" id="ddlRolAdic">                                                
            <option value=""> Seleccionar </option>                                                  
            <?php                                                  
              foreach ($roles as $key ) {
                   echo "<option value='".$key['idRol']."'>".$key['Nombre']."</option>";                                             
               }                                                  
            ?>                                            
            </select>                                       
      </div>                                        
      <div class="campoObligatorio">*</div>                                                                            
      </td>                                
      </tr>                                
                            
       </table>                        
       </fieldset>                    
       </div>                
       </div>            
       </div>        
       </div>        
       <div id="divError"></div>      
       <div class="clearBoth"></div>      
       <div class="divGuardar">          
       <button type="submit" class="submit">              
       <img src="/images/guardar.jpg" width="36" height="36" />              
       <br/> Guardar  </button>      
       </div>    
       </div>    
       <div id="tabModificar">        
       <div id="divConvenioModificar" class="clearfix">            
       <ul id="sidemenuMod">                
       <li>                    
       <a href="#informacion-contentMod" class="open"><i class="icon-home icon-large"></i>Informaci&oacute;n Modulo</a>                
       </li>            
       </ul>            
       <div id="contentMod">                
       <!--Tab Datos Basicos-->                
       <div id="informacion-contentMod" class="contentblock">                    
       <div align="center">                        
       <fieldset align="center">                          
       <legend align="left" class="legend">Datos B&aacute;sicos</legend>                            
       <table align="left">                                                                
       <tr>                                    
       <td align="left" class="tdFormulario">                                        
       <label>Nombre </label>                                    
       </td>                                    
       <td align="left" class="tdFormulario">                                        
       <div class="divControl">                                            
       <input type="text" <?php echo $soloLectura; ?> name="txtNombre" id="txtNombre" value="<?php echo utf8_decode($registro['Nombre']) ?>"/>                                            
       <input type="hidden" <?php echo $soloLectura; ?> name="txtId" id="txtId" value="<?php echo $registro['idModulo'] ?>"/>   
       </div>                                        
       <div class="campoObligatorio">*</div>                                                                            
       </td>                                
       </tr>                                
                                    
                                  
            </table>                                                      
            <div class='clearBoth'></div>                                                    
            </fieldset>                    
            </div>                
            </div>            
            </div>        
            </div>        
            <div id="divErrorMod"></div>      
            <div class="clearBoth"></div>      
            <div class="divGuardar">          
            <?php          
            if($tituloTab == 'Consulta')          
                {          
                    ?>              
                    <a href="/index.php/controladorModulo">                  
                    <img src="/images/volver.png" width="36" height="36" />                  
                    <br/> Regresar </a>           
                    <?php          
                    }          
                    else          
                        { 
                            ?>              
                            <div id="divGuardarMod">                  
                            <button type="submit" class="submit">                      
                            <img src="/images/guardar.jpg" width="36" height="36" />                      
                            <br/> Guardar  </button>              
                            </div>              
                            <div id="divRegresarMod">                  
                            <?php                                        
                            if($usuario['perfil'] != "Administracion")                        
                                echo '<a href="/index.php/controladorInicio">                                  
                            <img src="/images/volver.png" width="36" height="36" />                                  
                            <br/>  Regresar  </a>';                    
                            else                        
                                echo '<a href="/index.php/controladorModulo">                                              
                            <img src="/images/volver.png" width="36" height="36" />                                  
                            <br/>  Regresar </a>';                                      
                            ?>                            
                            </div>              
                            <div class="clearBoth"></div>          
                            <?php 
                            } 
                            ?>      
                            </div>    
                            </div>
                            </div>
                            <?php FinalDocumento(); ?>