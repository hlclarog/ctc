$(function(){       
    $("form").validate({
        rules: {            
          txtRutAdic:{
             required: false,
             digits: true,
             minlength: 6
          }, 
          txtNitadic:{
             required: true,
             digits: true,
             minlength: 6
          }, 
          txtDigitoVerificacionAdic:{
             required : false,
             digits: true,
             maxlength: 1
          },
          txtSiglaAdic:{
             required: true
          },                    
          txtNombreEmpresaAdic:{
             required: true
          },          
          sltClasificacionEconAdic: {
              required: false
          },
          sltDepartamentoAdic: {
              required: true
          },
          sltMunicipioAdic: {
              required: true
          },          
          sltGrupoEconomAdic: {
              required: false
          },      
          sltEmpresaSegOriCapAdic: {  
              required: false
          },          
          sltEmpresaSegCapAdic: {
              required: false
          },
          txtCorreoAdic:{
             required: false,
             email: true
          }
        },
        messages: {                                              
            txtRutAdic: { 
                required: "Escriba el RUT",
                digits: "Solo se permiten n�meros",
                minlength: "M�nimo 6 digitos"
            },
            txtNitadic: { 
                required: "Escriba el NIT",
                digits: "Solo se permiten n�meros",
                minlength: "M�nimo 6 digitos"
            },
            txtDigitoVerificacionAdic:{
                digits: "Solo se permiten n�meros",
                maxlength: "Maximo 1 digito"
            },
            txtNombreEmpresaAdic: "Escriba un nombre",
            txtSiglaAdic: "Escriba un sigla ",
            txtCorreoAdic: "Escriba un email valido",
            txtFechaUltInscrJunDirectivaAdic: "Escriba o seleccione una fecha valida",
            txtFechaAdic: "Escriba o seleccione una fecha valida",
            txtNumeroAfiliadosPorEmpresaAdic: "Solo se permiten n�meros",                        
            sltClasificacionEconAdic: "Seleccione una clasificacion economica",
            sltDepartamentoAdic: "Seleccione un departamento",
            sltMunicipioAdic: "Seleccione un municipio",
            sltGrupoEconomAdic: "Seleccione un grupo economico",
            sltEmpresaSegOriCapAdic: "Seleccione el tipo de capital",
            sltEmpresaSegCapAdic: "Seleccione capital de la empresa"

        }
    });
    
    $("#sltDepartamentoAdic").change(function(event){
        var id = $("#sltDepartamentoAdic").find(':selected').val();
        $("#sltMunicipioAdic").load('/index.php/controladorSindicato/ObtenerMunicipiosPorDepartamento/' + id);
    }); 
        
    $("input[name=txtNitadic]").change(function(){        
        var valor = $("#txtNitadic").val();
        $("#divNitVal").empty();
        $("#divNitVal").load('/index.php/controladorEmpresa/ValidarNit/' + valor);                
    });
    
    $('#txtOtroTipoEmpresaAdic').attr('disabled', 'disabled');                
    
    $("#sltEmpresaTipEstAdic").change(function(event){
        var codTipoViolacion = $("#sltEmpresaTipEstAdic").find(':selected').val();
        if(codTipoViolacion === "153")
            $('#txtOtroTipoEmpresaAdic').removeAttr('disabled');
        else
            $('#txtOtroTipoEmpresaAdic').attr('disabled', 'disabled');
        });                     
 
        
    $( "form" ).submit(function( event ) {
      var validaCampo = false;
            
      var existeNit = $('#divNitVal').is(':empty');      
      if(existeNit === false)
        validaCampo = true;
        
      if($("#txtNitadic").val() === "")                    
        validaCampo = true;
    
      if($("#txtNombreEmpresaAdic").val() === "")                    
        validaCampo = true;    
    
      if($("#sltDepartamentoAdic").val() === "")                    
        validaCampo = true;
            
      if($("#sltMunicipioAdic").val() === "")                    
        validaCampo = true;          
              
      if($("#sltClasificacionEconAdic").val() === "")                    
        validaCampo = false;
          
      if($("#sltGrupoEconomAdic").val() === "")                    
        validaCampo = true;                    
    
      if($("#sltEmpresaSegOriCapAdic").val() === "")                    
        validaCampo = false;                        
    
      if($("#sltEmpresaSegCapAdic").val() === "")                    
        validaCampo = false;                            
    
      if(validaCampo === false)
      {
        $( "#divError" ).text( "Cargando..." ).show();
        return;
      }
      else      
      {
        if(existeRut === true)
            $( "#divError" ).text( "El NIT ya existe, escriba uno nuevo." ).show();
        else
            $( "#divError" ).text( "Falta diligenciar campos obligatorios, revisar todas las pesta�as." ).show();
        
        event.preventDefault();        
      }
    });
});