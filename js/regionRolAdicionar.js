$(function(){    
   
    $("form").validate({
        rules: {
          ddlRegionAdic:{
             required: true
          } 
        },
        messages: {            
            ddlRegionAdic: "Seleccione una región" 
        }
    });
       
    
    $("input[name=txtUsuarioAdic]").change(function(){        
        var valor = $("#txtUsuarioAdic").val();
        $("#divValUsuario").empty();
        $("#divValUsuario").load('/index.php/controladorUsuario/ValidarRol/' + valor);                
    });    
    
    $( "form" ).submit(function( event ) {
      var validaCampo = false;
      
      var existeRegion = $('#divValUsuario').is(':empty');      
      if(existeRegion === false)
        validaCampo = true;
       
      if(validaCampo === false)
      {
        $( "#divError" ).text( "Cargando..." ).show();
        return;
      }
      else      
      { if(existeUsuario === false)
            $( "#divError" ).text( "La región ya esta asignado al rol, Seleccione una nueva región." ).show();             
         else
            $( "#divError" ).text( "Falta diligenciar campos obligatorios, revisar todas las pestañas." ).show();
        
        event.preventDefault();        
      }
    });
});