$(function(){    
   
    $("form").validate({
        rules: {
          ddlRolAdic:{
             required: true
          } 
        },
        messages: {            
            ddlRolAdic: "Seleccione el rol" 
        }
    });
       
    
    $("input[name=txtUsuarioAdic]").change(function(){        
        var valor = $("#txtUsuarioAdic").val();
        $("#divValUsuario").empty();
        $("#divValUsuario").load('/index.php/controladorUsuario/ValidarRol/' + valor);                
    });    
    
    $( "form" ).submit(function( event ) {
      var validaCampo = false;
      
      var existeRol = $('#divValUsuario').is(':empty');      
      if(existeRol === false)
        validaCampo = true;
       
      if(validaCampo === false)
      {
        $( "#divError" ).text( "Cargando..." ).show();
        return;
      }
      else      
      { if(existeUsuario === false)
            $( "#divError" ).text( "El usuario ya tiene este rol, seleccione uno nuevo." ).show();             
         else
            $( "#divError" ).text( "Falta diligenciar campos obligatorios, revisar todas las pestañas." ).show();
        
        event.preventDefault();        
      }
    });
});