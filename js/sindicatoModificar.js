$(function(){    
    $("form").validate({
        rules: {
          txtFecha: {
            required: false,
            date: true
          },
          txtFechaUltInscrJunDirectiva: {
            required: false,
            date: true
          },
          txtCorreo:{
             required: false,
             email: true
          },
          txtNumeroResolucion:{
             required: false
             
          },
          txtNombSindicato:{
             required: true
          },
          txtSigla:{
             required: true
          },
          sltClaseDirectiva: {
              required: false
          },
          sltCodDepartamento: {
              required: true
          },
          sltCodMunicipio: {
              required: true
          },
          sltFederacionAfiliacion: {
              required: true
          },                    
          sltPeriodoVigJuntaDirectiva: {
              required: true
          },
          txtNumeroAfiliadosEmpresa: {
              required: false,
              digits: true
          },
          txtNumeroAfiliadosActuales: {
              required: false,
              digits: true
          },
          txtNumeroAfiliadosHombres: {
              required: false,
              digits: true
          },
          txtNumeroAfiliadosMujeres: {
              required: false,
              digits: true
          },
          txtNumeroAfiliadosJovenes35: {
              required: false,
              digits: true
          },
          txtNumeroAfiliadosSectorFormal: {
              required: false,
              digits: true
          },
          txtNumeroAfiliadosSectorInformal: {
              required: false,
              digits: true
          },
          sltEstado: {
              required: true
          },  
          txtNumeroTotalAfiliados: {
              required: false,
              digits: true
          },
          txtDescripcionAfiliadosEmpresa: {
              required: false,
              digits: false
          },
          sltBienesInmueblesProp: {
              required: true
          },
          txtFechaUltimaActualizacion: {
            required: false,
            date: true
          },
          sltClaseSindicato: {
              required: true
          },
          sltSindicatoSegOriCap: {
              required: true
          },
          sltSindicatoSegTipEmprEst: {
              required: true
          },
          sltSindicatoEstModaContra: {
              required: true
          },
          sltClasificacionEconSind: {
              required: true
          },
          sltAfiliacionFederacionRegionalSec:{
              required: true
          },
          sltAfiliacionFedRama: {
              required: true
          },
          sltCentralSindProv: {
              required: true
          },
          sltAfiliacionInt:{
              required: true   
          },
          txtTelefono : { 
              minlength: 7,               
              number: true              
          },          
          txtCelular : { 
              minlength: 10,               
              number: true              
          },          
          txtFax : { 
              minlength: 7,               
              number: true              
          }                    
        },
        messages: {            
            txtCorreo: "Escriba un email valido",
            txtFechaUltInscrJunDirectiva: "Escriba o seleccione una fecha valida",
            txtFecha: "Escriba o seleccione una fecha valida",
            txtNumeroResolucion: { 
                required: "Escriba un n�mero de resoluci�n"
                
            },
            txtNombSindicato: "Escriba un nombre",
            txtSigla: "Escriba una sigla",
            sltClaseDirectiva: "Seleccione una clase",
            sltCodDepartamento: "Seleccione un departamento",
            sltCodMunicipio: "Seleccione un municpio",
            sltPeriodoVigJuntaDirectiva: "Seleccione un periodo de vigencia",
            txtNumeroAfiliadosEmpresa: "Solo se permiten n�meros",
            txtNumeroAfiliadosActuales: "Solo se permiten n�meros",
            txtNumeroAfiliadosHombres: "Solo se permiten n�meros",
            txtNumeroAfiliadosMujeres: "Solo se permiten n�meros",
            txtNumeroAfiliadosJovenes35: "Solo se permiten n�meros",
            txtNumeroAfiliadosSectorFormal: "Solo se permiten n�meros",
            txtNumeroAfiliadosSectorInformal: "Solo se permiten n�meros",
            txtDescripcionAfiliadosEmpresa: "Solo se permiten n�meros",
            txtNumeroTotalAfiliados: "Solo se permiten n�meros",
            sltEstado: "Seleccione un estado",            
            sltBienesInmueblesProp: "Seleccione un bien inmueble",
            txtFechaUltimaActualizacion: "Escriba o seleccione una fecha valida",
            sltClaseSindicato: "Seleccione una clase de sindicato",
            sltFederacionAfiliacion: "Seleccione la federaci�n de afiliaci�n ",
            sltSindicatoSegOriCap: "Seleccione un sindicato seg�n origen",
            sltSindicatoSegTipEmprEst: "Seleccione un sindicato seg�n tipo de empresa",
            sltSindicatoEstModaContra: "Seleccione un sindicato estatal seg�n modalidad",
            sltClasificacionEconSind: "Seleccione una clasificaci�n econ�mica",
            sltAfiliacionFederacionRegionalSec: "Seleccione afiliaci�n regional",
            sltAfiliacionFedRama: "Seleccione una afiliaci�n a federaci�n",
            sltCentralSindProv: "Seleccione una central",
            sltAfiliacionInt: "Selecciona una afiliaci�n",
            txtTelefono : { 
                minlength: "El n�mero de tel�fono introducido no es correcto.",
                number: "Solo se permiten n�meros"
            },
            txtCelular : { 
                minlength: "El n�mero de Celular introducido no es correcto.",
                number: "Solo se permiten n�meros"
            },
            txtFax : { 
                minlength: "El n�mero de Fax introducido no es correcto.",
                number: "Solo se permiten n�meros"
            }                               
        }
    });
    
    $("#sltCodDepartamento").change(function(event){
        var id = $("#sltCodDepartamento").find(':selected').val();
        $("#sltCodMunicipio").load('/index.php/controladorSindicato/ObtenerMunicipiosPorDepartamento/' + id);
    });    

    $('#txtFechaUltimaActualizacion').attr('disabled', 'disabled');

    $(function() {  Estado();
    $("#sltEstado").click(Estado);});
    
    function Estado() {
    if($("#sltEstado").val() === "INACTIVO") {
        
        $('#sltCaracteristicasSindicatoInactivo').removeAttr('disabled');
        
    } else {
      
        $('#sltCaracteristicasSindicatoInactivo').attr('disabled', 'disabled');
          }
    }   
    
    $(function() {  Fusionado();
    $("#sltCaracteristicasSindicatoInactivo").click(Fusionado);});
    
    function Fusionado() {
    if($("#sltCaracteristicasSindicatoInactivo").val() === "SINDICATOFUSIONADO") {
        
        $('#txtNombreSindicatoFusiona').removeAttr('disabled');
        
    } else {
      
        $('#txtNombreSindicatoFusiona').attr('disabled', 'disabled');
          }
    }           
    
   $(function() {  OtrosBienesInmuebles();
   $("#chkOtrosBienesInmuebles").click(OtrosBienesInmuebles);});

    function OtrosBienesInmuebles() {
    if (this.checked || $("#chkOtrosBienesInmuebles").attr('checked')  ) {
      $('#txtOtrosBienesInmubles').removeAttr('disabled');         
        
    } else {
      
      $('#txtOtrosBienesInmubles').attr('disabled', 'disabled');                
          }
    }        
    
   $(function() {  Otras_secretaria();
   $("#chkOtraSecretaria").click(Otras_secretaria);});

    function Otras_secretaria() {
    if (this.checked || $("#chkOtraSecretaria").attr('checked')  ) {
      $('#txtOtraSecretaria').removeAttr('disabled');         
        
    } else {
      
      $('#txtOtraSecretaria').attr('disabled', 'disabled');                
          }
    }    
    
    $(function() {  tipoViolenciaSi();
    $("#rdbVictimaViolenciaSi").click(tipoViolenciaSi);});

    function tipoViolenciaSi() {
    if (this.checked || $("#rdbVictimaViolenciaSi").attr('checked')  ) {        
        
        $('#chkAllanamientoIlegal').removeAttr('disabled');
        $('#chkAmenazas').removeAttr('disabled');
        $('#chkAtentadoLesiones').removeAttr('disabled');
        $('#chkDesaparicion').removeAttr('disabled');
        $('#chkDesplazamientoForzoso').removeAttr('disabled');
        $('#chkDetencionArbitraria').removeAttr('disabled');
        $('#chkHomicidios').removeAttr('disabled');
        $('#chkHostigamiento').removeAttr('disabled');
        $('#chkSecuestro').removeAttr('disabled');
        $('#chkOtroTipoViolencia').removeAttr('disabled');        
        
        
        } else {
                                   
            $('#chkAllanamientoIlegal').attr('disabled', 'disabled');
            $('#chkAmenazas').attr('disabled', 'disabled');
            $('#chkAtentadoLesiones').attr('disabled', 'disabled');
            $('#chkDesaparicion').attr('disabled', 'disabled');
            $('#chkDesplazamientoForzoso').attr('disabled', 'disabled');
            $('#chkDetencionArbitraria').attr('disabled', 'disabled');
            $('#chkHomicidios').attr('disabled', 'disabled');
            $('#chkHostigamiento').attr('disabled', 'disabled');
            $('#chkSecuestro').attr('disabled', 'disabled');
            $('#chkOtroTipoViolencia').attr('disabled', 'disabled');
        
          }
    }               
    
    $(function() {  tipoViolenciaNo();
    $("#rdbVictimaViolenciaNo").click(tipoViolenciaNo);});

    function tipoViolenciaNo() {
    if (this.checked) {
                
            $('#chkAllanamientoIlegal').attr('disabled', 'disabled');
            $('#chkAmenazas').attr('disabled', 'disabled');
            $('#chkAtentadoLesiones').attr('disabled', 'disabled');
            $('#chkDesaparicion').attr('disabled', 'disabled');
            $('#chkDesplazamientoForzoso').attr('disabled', 'disabled');
            $('#chkDetencionArbitraria').attr('disabled', 'disabled');
            $('#chkHomicidios').attr('disabled', 'disabled');
            $('#chkHostigamiento').attr('disabled', 'disabled');
            $('#chkSecuestro').attr('disabled', 'disabled');
            $('#chkOtroTipoViolencia').attr('disabled', 'disabled');
        } 
    }                   
    
    $(function() {  OtroTipoViolencia();
    $("#chkOtroTipoViolencia").click(OtroTipoViolencia);});

    function OtroTipoViolencia() {
    if (this.checked || $("#chkOtroTipoViolencia").attr('checked')  ) {
      $('#txtOtroTipoViolencia').removeAttr('disabled');         
        
    } else {
      
      $('#txtOtroTipoViolencia').attr('disabled', 'disabled');                
          }
    }        
        
    $( "form" ).submit(function( event ) {
      var validaCampo = false;
      
      if($("#txtNumeroResolucion").val() === "")              
        validaCampo = false;      
              
      if($("#txtRut").val() === "")                    
        validaCampo = false;
          
      if($("#sltClaseDirectiva").val() === "")              
        validaCampo = false;
    
      
      if($("#sltEstado").val() === "")                   
        validaCampo = false;      
      
      var codEstado = $("#sltEstado").find(':selected').val();
      if(codEstado === "INACTIVO" && $("#sltCaracteristicasSindicatoInactivo").val() === "")
          validaCampo = false;
                   
      if($("#sltBienesInmueblesProp").val() === "")                    
        validaCampo = false;
      
      var codBien = $("#sltBienesInmueblesProp").find(':selected').val();
      if(codBien === "OTROSBIENESINMUEBLES" && $("#txtOtrosBienesInmubles").val() === "")
          validaCampo = false;
      
      if($("#sltClaseSindicato").val() === "")                    
        validaCampo = false;
            
      if($("#sltSindicatoSegOriCap").val() === "")                    
        validaCampo = false;
            
      if($("#sltSindicatoSegTipEmprEst").val() === "")                    
        validaCampo = false;
    
      if($("#sltFederacionAfiliacion").val() === "")                    
        validaCampo = false;          
      
      if($("#sltSindicatoEstModaContra").val() === "")              
        validaCampo = false;
            
      if($("#sltClasificacionEconSind").val() === "")                    
        validaCampo = false;      
      
      if($("#sltAfiliacionFederacionRegionalSec").val() === "")                    
        validaCampo = false;
            
      if($("#sltAfiliacionFedRama").val() === "")                    
        validaCampo = false;      
      
      if($("#sltCentralSindProv").val() === "")                    
        validaCampo = false;      
       
      if($("#sltAfiliacionInt").val() === "")                    
        validaCampo = false;  

      if($("#txtNombSindicato").val() === "")              
        validaCampo = true;   

      if($("#sltCodDepartamento").val() === "")                    
        validaCampo = true;
            
      if($("#sltCodMunicipio").val() === "")                    
        validaCampo = true;       
      
      if(validaCampo === false)
      {
        $( "#divErrorMod" ).text( "Cargando..." ).show();
        return;
      }
      else      
      {
        if(codBien === "OTROSBIENESINMUEBLES" && $("#txtOtrosBienesInmubles").val() === "")
        {            
            //var idx = $('#tabAdicionar a[href="#about-content"]').parent().index();
            //$("#tabAdicionar").tabs( "option", "active", idx);
            var index = $('#tabModificar a[href="#about-contentMod"]').parent().index();
            $('#tabModificar').tabs({ active: index });
            $("#txtOtrosBienesInmubles").css({'border': "1px solid #A30000"});
            $('#txtOtrosBienesInmubles').focus();            
            $( "#divErrorMod" ).text( "Falta diligenciar el campo Otros Bienes Inmuebles de la pesta�a informaci�n administrativa." ).show();
        }
        else if(codEstado === "INACTIVO" && $("#sltCaracteristicasSindicatoInactivo").val() === "")
        {   
            var index = $('#tabModificar a[href="#about-contentMod"]').parent().index();
            $('#tabModificar').tabs({ active: index });
            $("#sltCaracteristicasSindicatoInactivo").css({'border': "1px solid #A30000"});
            $('#sltCaracteristicasSindicatoInactivo').focus();            
            $( "#divErrorMod" ).text( "Falta diligenciar el campo Caracter�stica  para  Sindicato Inactivo de la pesta�a informaci�n administrativa." ).show();
        }
        else
            $( "#divErrorMod" ).text( "Falta diligenciar campos obligatorios, revisar todas las pesta�as." ).show();
        
        event.preventDefault();        
      }
    });
});