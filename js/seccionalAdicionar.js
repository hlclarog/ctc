$(function(){    
   
    $("form").validate({
        rules: {
          txtRutAdic:{
             required: false,
             digits: true,
             minlength: 6
          },
          txtDigitoVerificacionAdic:{
             required: false,
             digits: true,
             maxlength: 1
          },
          txtNitadic:{
            required: true,
          },
          txtRegistroSindicalAdic:{
            required: true 
          },
          txtNumeroResolucionAdic:{
             required: false
          },            
          txtFechaAdic: {
            required: false,
            date: true
          },
          txtNombreSeccionalAdic:{
             required: true
          },
          sltDepartamentoAdic: {
              required: true
          },
          sltMunicipioAdic: {
              required: true
          },
          txtCorreoAdic:{
             required: false,
             email: true
          },                    
          sltTipoFederacionAdic: {
              required: false
          },          
          txtFechaUltInscrJunDirectivaAdic: {
            required: false,
            date: true
          },
          sltPeriodoVigJuntaDirectivaAdic: {
              required: false
          },
          sltBienesInmueblesPropAdic: {
              required: false
          },
          txtFechaUltActualizacionInfAdic: {
            required: false,
            date: true
          },
          sltAfiliacionIntAdic:{
              required: true   
          },
          txtTelefonoAdic: { 
              minlength: 7,               
              number: true              
          },          
          txtCelularAdic: { 
              minlength: 10,               
              number: true              
          },          
          txtFaxAdic: { 
              minlength: 7,               
              number: true              
          },
          txtSiglaAdic:{
             required:true
          }                 

        },
        messages: {            
            txtRutAdic: { 
                required: "Escriba el RUT",
                digits: "Solo se permiten n�meros",
                minlength: "M�nimo 6 digitos"
            },
            txtDigitoVerificacionAdic: { 
                required: "Escriba el Digito de Verificaci�n ",
                digits: "Solo se permiten n�meros",
                maxlength: "Maximo 1 digitos"
            },
            txtNitadic:{
              required:"Escriba el Nit",
            },                        
            txtNumeroResolucionAdic: { 
                required: "Escriba un n�mero de resoluci�n",
            }, 
            txtRegistroSindicalAdic :{
              required: "Escriba el numero de registro sindical",
            },                       
            txtFechaAdic: "Escriba o seleccione una fecha valida",            
            txtNombreSeccionalAdic: "Escriba un nombre",
            sltDepartamentoAdic: "Seleccione un departamento",
            sltMunicipioAdic: "Seleccione un municpio",
            txtCorreoAdic: "Escriba un email valido",
            sltTipoFederacionAdic: "Seleccione un tipo",            
            txtFechaUltInscrJunDirectivaAdic: "Escriba o seleccione una fecha valida",                                               
            sltPeriodoVigJuntaDirectivaAdic: "Seleccione un periodo de vigencia",
            sltBienesInmueblesPropAdic: "Seleccione un bien inmueble",
            txtFechaUltActualizacionInfAdic: "Escriba o seleccione una fecha valida",
            sltAfiliacionIntAdic: "Selecciona una afiliaci�n",
            txtTelefonoAdic : { 
                minlength: "El n�mero de tel�fono introducido no es correcto.",
                number: "Solo se permiten n�meros"
            },
            txtCelularAdic : { 
                minlength: "El n�mero de Celular introducido no es correcto.",
                number: "Solo se permiten n�meros"
            },
            txtFaxAdic : { 
                minlength: "El n�mero de Fax introducido no es correcto.",
                number: "Solo se permiten n�meros"
            } ,
            txtSiglaAdic: "Escriba sigla"           
        }
    });
     
    $("input[name=txtRutAdic]").change(function(){        
        var valor = $("#txtRutAdic").val();
        $("#divRutVal").empty();
        $("#divRutVal").load('/index.php/controladorFederacion/ValidarRut/' + valor);                
    });
    
    $("input[name=txtRegistroSindicalAdic]").change(function(){        
        var valor = $("#txtRegistroSindicalAdic").val();
        $("#divRegistroSindicalVal").empty();
        $("#divRegistroSindicalVal").load('/index.php/controladorSeccional/ValidardivRegistroSindicalVal/' + valor);                
    });            
    
    $("#sltDepartamentoAdic").change(function(event){
        var id = $("#sltDepartamentoAdic").find(':selected').val();
        $("#sltMunicipioAdic").load('/index.php/controladorSindicato/ObtenerMunicipiosPorDepartamento/' + id);
    }); 
    
   $(function() {  OtrosBienesInmuebles();
   $("#chkOtrosBienesInmueblesAdic").click(OtrosBienesInmuebles);});

    function OtrosBienesInmuebles() {
    if (this.checked) {
        
        $('#txtOtrosBienesInmueblesAdic').removeAttr('disabled');
        
    } else {
      
        $('#txtOtrosBienesInmueblesAdic').attr('disabled', 'disabled');
          }
    }           
    
   $(function() {  Otras_secretaria();
   $("#chkOtraSecretariaAdic").click(Otras_secretaria);});

    function Otras_secretaria() {
    if (this.checked) {
        
        $('#txtOtraSecretariaAdic').removeAttr('disabled');
        
    } else {
      
        $('#txtOtraSecretariaAdic').attr('disabled', 'disabled');
          }
    }

    $(function() {  Estado();
    $("#sltEstadoAdic").click(Estado);});
    
    function Estado() {
    if($("#sltEstadoAdic").val() === "INACTIVO") {
        
        $('#sltCaracteristicasSeccionalInactivoAdic').removeAttr('disabled');
        
    } else {
      
        $('#sltCaracteristicasSeccionalInactivoAdic').attr('disabled', 'disabled');
          }
    }   
    
    $(function() {  Fusionado();
    $("#sltCaracteristicasSeccionalInactivoAdic").click(Fusionado);});
    
    function Fusionado() {
    if($("#sltCaracteristicasSeccionalInactivoAdic").val() === "SECCIONALFUSIONADA") {
        
        $('#txtNombreSeccionalFusionaAdic').removeAttr('disabled');
        
    } else {
      
        $('#txtNombreSeccionalFusionaAdic').attr('disabled', 'disabled');
          }
    }               
    
    $( "form" ).submit(function( event ) {
      var validaCampo = false;
      
      var existeRut = $('#divRutVal').is(':empty');      
      if(existeRut === false)
        validaCampo = true;
    
      if($("#txtRutAdic").val() === "")                    
        validaCampo = false;
    
      if($("#txtNumeroResolucionAdic").val() === "")              
        validaCampo = false;      
    
      if($("#txtDigitoVerificacionAdic").val() === "")                    
        validaCampo = false;        
              
      if($("#sltDepartamentoAdic").val() === "")                    
        validaCampo = false;
            
      if($("#sltMunicipioAdic").val() === "")                    
        validaCampo = false;      
      
      if($("#sltTipoFederacionAdic").val() === "")              
        validaCampo = false;
    
      if($("#sltPeriodoVigJuntaDirectivaAdic").val() === "")                    
        validaCampo = false;
                         
      if($("#sltBienesInmueblesPropAdic").val() === "")                    
        validaCampo = false;
      
      var codBien = $("#sltBienesInmueblesPropAdic").find(':selected').val();
      if(codBien === "OTROSBIENESINMUEBLES" && $("#txtOtrosBienesInmueblesAdic").val() === "")
          validaCampo = false;
                  
      if($("#sltAfiliacionIntAdic").val() === "")                    
        validaCampo = false;   

      if($("#txtNombreFederacionAdic").val() === "")              
        validaCampo = true;   
      
      if(validaCampo === false)
      {
        $( "#divError" ).text( "Cargando..." ).show();
        return;
      }
      else      
      {
        if(existeRut === false)
            $( "#divError" ).text( "El RUT ya existe, escriba uno nuevo." ).show();
        else if(codBien === "OTROSBIENESINMUEBLES" && $("#txtOtrosBienesInmueblesAdic").val() === "")
        {            
            //var idx = $('#tabAdicionar a[href="#about-content"]').parent().index();
            //$("#tabAdicionar").tabs( "option", "active", idx);
            var index = $('#tabAdicionar a[href="#about-content"]').parent().index();
            $('#tabAdicionar').tabs({ active: index });
            $("#txtOtrosBienesInmueblesAdic").css({'border': "1px solid #A30000"});
            $('#txtOtrosBienesInmueblesAdic').focus();            
            $( "#divError" ).text( "Falta diligenciar el campo Otros Bienes Inmuebles de la pesta�a informaci�n administrativa." ).show();
        }
        else
            $( "#divError" ).text( "Falta diligenciar campos obligatorios, revisar todas las pesta�as." ).show();
        
        event.preventDefault();        
      }
    });
});