$(function(){       
    $("form").validate({
        rules: {
          txtCedulaAdic:{
             required: true,
             digits: true,
             minlength: 6
          },            
          txtFechaIngresoEmpresaAdic: {
            required: false,
            date: true
          },
          txtFechaIngresoSindiAdic: {
            required: false,
            date: true
          },
          txtCorreoAdic:{
             required: false,
             email: true
          },
          txtNumeroHijosAdic:{
             required: false,
             digits: true
          },

          txtNombreAfliliadoAdic:{
             required: true  
          },

          sltGeneroAdic: {
              required: false
          },
          sltEpsAdic: {
              required: false 
          },          
          sltFondoPensionesAdic: {
              required: false
          },          
          sltFondoCesantiasAdic: {
              required: false 
          },          
          sltArlAdic: {
              required: false
          },          
          sltCajaCompensacionAdic: {
              required: false
          },                    
          sltDepartamentoAdic: {
              required: false
          },
          sltDepartamentoResiAdic: {
              required: false
          },
          sltDepartamentoLaboraAdic: {
              required: false
          },          
          sltMunicipioAdic: {
              required: false
          },
          sltMunicipioResiAdic: {
              required: false
          },          
          sltMunicipioLaboraAdic: {
              required: false
          },                    
          sltEstadoCivilAdic: {
              required: false
          },
          txtNumeroPreescolarAdic: {
              required: false,
              digits: true
          },
          txtNumeroPrimariaAdic: {
              required: false,
              digits: true
          },
          txtNumeroSecundariaAdic: {
              required: false,
              digits: true
          },
          txtNumerotecnicaAdic: {
              required: false,
              digits: true
          },
          txtNumeroTecnologiaAdic: {
              required: false,
              digits: true
          },
          txtNumeroUniversidadAdic: {
              required: false,
              digits: true
          },
          txtNumeropersonasCargoAdic: {
              required: false,
              digits: true
          },
          txtNumeroHorasTrabajoAdic: {
              required: false,
              digits: true
          },          
          txtPorcentajeCuotaSindicalAdic: {
              required: false,
              digits: true
          },                    
          txtSalarioBasicoAdic: {
              required: false,
              digits: true
          },                              
          sltNivelEducativoAdic: {
              required: false
          },
          txtFechaRetiroSindiAdic: {
            required: false,
            date: true
          },
          sltTipoViviendaAdic: {
              required: false
          },
          sltTipoAfiliadoAdic: {
              required: false
          },
          sltEmpresaDondeLaboraAdic: {
              required: true
          },
          sltCargoEmpresaPrivadaAdic: {
              required: false
          },
          sltCargoSectorPublicoAdic: {
              required: false
          },
          sltModalidadContratoAdic:{
              required: false
          },
          sltDuracionContratoLaboralAdic: {
              required: false
          },
          sltServidorPublicoAdic: {
              required: false
          },
          sltSalarioRangosAdic: {
              required: false
          },          
          sltOrganizacionSocialAdic: {
              required: false
          },                    
          sltCargoJuntaDirectivaAdic: {
              required: false
          },                    
          sltMiembroJuntaDirectivaAdic: {
              required: false
          },                    
          sltCondicionAfiliacionAdic: {
              required: false
          },                    
          sltContratacionEmpleoPublicoAdic:{
              required: false    
          },
          txtTelefonoAdic: { 
              minlength: 7,               
              number: true              
          },          
          txtCelularAdic: { 
              minlength: 10,               
              number: true              
          }                    
          
        },
        messages: {            
            txtCorreoAdic: "Escriba un email valido",
            txtFechaIngresoSindiAdic: "Escriba o seleccione una fecha valida",
            txtFechaIngresoEmpresaAdic: "Escriba o seleccione una fecha valida",
            txtNumeroHijosAdic: { 
                required: "Escriba el n�mero de hijos",
                digits: "Solo se permiten n�meros"
            },
            txtCedulaAdic: { 
                required: "Escriba la Cedula",
                digits: "Solo se permiten n�meros",
                minlength: "M�nimo 6 digitos"
            },
            txtNombreAfliliadoAdic: "Escriba un nombre",            
            sltGeneroAdic: "Seleccione un genero",
            sltEpsAdic: "Seleccione la EPS",
            sltFondoPensionesAdic: "Seleccione el Fondo de Pensiones",
            sltFondoCesantiasAdic: "Seleccione el Fondo de Cesantias",
            sltArlAdic: "Seleccione la Aseguradora Riesgos Laborales",
            sltCajaCompensacionAdic: "Seleccione la Caja de Compensacion",
            sltDepartamentoAdic: "Seleccione un departamento",
            sltDepartamentoResiAdic: "Seleccione un departamento",
            sltDepartamentoLaboraAdic: "Seleccione un departamento",
            sltMunicipioAdic: "Seleccione un municpio",
            sltMunicipioResiAdic: "Seleccione un municpio",
            sltMunicipioLaboraAdic: "Seleccione un municpio",
            sltEstadoCivilAdic: "Seleccione estado civil",
            txtNumeroPreescolarAdic: "Solo se permiten n�meros",
            txtNumeroPrimariaAdic: "Solo se permiten n�meros",
            txtNumeroSecundariaAdic: "Solo se permiten n�meros",
            txtNumerotecnicaAdic: "Solo se permiten n�meros",
            txtNumeroTecnologiaAdic: "Solo se permiten n�meros",
            txtNumeroUniversidadAdic: "Solo se permiten n�meros",
            txtNumeropersonasCargoAdic: "Solo se permiten n�meros",            
            txtNumeroHorasTrabajoAdic: "Solo se permiten n�meros",
            txtPorcentajeCuotaSindicalAdic: "Solo se permiten n�meros",
            txtSalarioBasicoAdic: "Solo se permiten n�meros",
            sltNivelEducativoAdic: "Seleccione el nivel educativo",
            txtFechaRetiroSindiAdic: "Escriba o seleccione una fecha valida",
            sltTipoViviendaAdic: "Seleccione el tipo de vivienda",
            sltTipoAfiliadoAdic: "Seleccione el tipo de afiliado",
            sltEmpresaDondeLaboraAdic: "Seleccione la empresa donde labora",
            sltCargoEmpresaPrivadaAdic: "Seleccione el cargo en empresa privada",
            sltCargoSectorPublicoAdic: "Seleccione el cargo en sector publico",
            sltModalidadContratoAdic: "Seleccione la modalidad de contrato",
            sltDuracionContratoLaboralAdic: "Seleccione la duracion de contrato laboral",
            sltServidorPublicoAdic: "Seleccione el tipo de servidor publico",
            sltSalarioRangosAdic: "Seleccione el salario basico por rango",            
            sltOrganizacionSocialAdic: "Seleccione si pertenencia a otra organizaci�n social",            
            sltCargoJuntaDirectivaAdic: "Seleccione el cargo en junta directiva",            
            sltCondicionAfiliacionAdic: "Seleccione la condicion de afiliacion",            
            sltMiembroJuntaDirectivaAdic: "Seleccione miembro junta directiva",            
            sltContratacionEmpleoPublicoAdic: "Selecciona la modalidad de contratacion de empleo publico",
            txtTelefonoAdic : { 
                minlength: "El n�mero de tel�fono introducido no es correcto.",
                number: "Solo se permiten n�meros"
            },
            txtCelularAdic : { 
                minlength: "El n�mero de Celular introducido no es correcto.",
                number: "Solo se permiten n�meros"
            }            
            
        }
    });
    
    $('#txtTituloProfesiAdic').attr('disabled', 'disabled');
    
    $("#sltNivelEducativoAdic").change(function(event){
        var codBien = $("#sltNivelEducativoAdic").find(':selected').val();
        if(codBien === "NIVELEDUCATIVO")
            $('#txtTituloProfesiAdic').removeAttr('disabled');
        else
            $('#txtTituloProfesiAdic').attr('disabled', 'disabled');
    }); 
    
    $("#sltDepartamentoAdic").change(function(event){
        var id = $("#sltDepartamentoAdic").find(':selected').val();
        $("#sltMunicipioAdic").load('/index.php/controladorAfiliado/ObtenerMunicipiosPorDepartamento/' + id);
    }); 
    
    $("#sltDepartamentoResiAdic").change(function(event){
        var id = $("#sltDepartamentoResiAdic").find(':selected').val();
        $("#sltMunicipioResiAdic").load('/index.php/controladorAfiliado/ObtenerMunicipiosPorDepartamento/' + id);
    }); 
    
    $("#sltDepartamentoLaboraAdic").change(function(event){
        var id = $("#sltDepartamentoLaboraAdic").find(':selected').val();
        $("#sltMunicipioLaboraAdic").load('/index.php/controladorAfiliado/ObtenerMunicipiosPorDepartamento/' + id);
    });     
        
    $("input[name=txtCedulaAdic]").change(function(){        
        var valor = $("#txtCedulaAdic").val();
        $("#divCedulaVal").empty();
        $("#divCedulaVal").load('/index.php/controladorAfiliado/ValidarCedula/' + valor);                
    });
    
    
    
   $(function() {  EstudiaSi();
   $("#rdbEstudiaActualmenteSiAdic").click(EstudiaSi);});

    function EstudiaSi() {
    if (this.checked) {
        
        $('#txtQueEstudiaAdic').removeAttr('disabled');
        
        } else {
      
        $('#txtQueEstudiaAdic').attr('disabled', 'disabled');
          }
    }           
    
    $(function() {  EstudiaNo();
   $("#rdbEstudiaActualmenteNoAdic").click(EstudiaNo);});

    function EstudiaNo() {
    if (this.checked) {
                
        $('#txtQueEstudiaAdic').attr('disabled', 'disabled');
        
        } 
    }           
    
    $(function() {  JuntaDirectiva();
    $("#sltMiembroJuntaDirectivaAdic").click(JuntaDirectiva);});
    
    function JuntaDirectiva() {
    if($("#sltMiembroJuntaDirectivaAdic").val() === "MIEMBJUNDIRECTIVANO") {
        
        $('#sltCargoJuntaDirectivaAdic').attr('disabled', 'disabled');        
        
    } else {
        $('#sltCargoJuntaDirectivaAdic').removeAttr('disabled');        
          }
    }       
    
    $(function() {  CreditoSi();
    $("#rdbEntidadCreditoSiAdic").click(CreditoSi);});

    function CreditoSi() {
    if (this.checked) {
        
        $('#txtEntidadCreditoAdic').removeAttr('disabled');
        
        } else {
      
        $('#txtEntidadCreditoAdic').attr('disabled', 'disabled');
          }
    }           
    
    $(function() {  CreditoNo();
    $("#rdbEntidadCreditoNoAdic").click(CreditoNo);});

    function CreditoNo() {
    if (this.checked) {
                
        $('#txtEntidadCreditoAdic').attr('disabled', 'disabled');
        
        } 
    }               
    
    $(function() {  OtroSindicatoSi();
    $("#rdbPerteneceOtroSindicatoSiAdic").click(OtroSindicatoSi);});

    function OtroSindicatoSi() {
    if (this.checked) {
        
        $('#txtOtroSindicatoAfiliacionAdic').removeAttr('disabled');
        
        } else {
      
        $('#txtOtroSindicatoAfiliacionAdic').attr('disabled', 'disabled');
          }
    }           
    
    $(function() {  OtroSindicatoNo();
    $("#rdbPerteneceOtroSindicatoNoAdic").click(OtroSindicatoNo);});

    function OtroSindicatoNo() {
    if (this.checked) {
                
        $('#txtOtroSindicatoAfiliacionAdic').attr('disabled', 'disabled');
        
        } 
    }                   
        
    $( "form" ).submit(function( event ) {
      var validaCampo = false;
            
      var existeCedula = $('#divCedulaVal').is(':empty');      
      if(existeCedula === false)
        validaCampo = true;
    
      if($("#txtNumeroHijosAdic").val() === "")              
        validaCampo = false;      
      
      
          
      if($("#sltGeneroAdic").val() === "")              
        validaCampo = false;
    
      if($("#sltEpsAdic").val() === "")              
        validaCampo = false;    
    
      if($("#sltArlAdic").val() === "")              
        validaCampo = false;        
    
      if($("#sltCajaCompensacionAdic").val() === "")              
        validaCampo = false;        
    
      if($("#sltFondoCesantiasAdic").val() === "")              
        validaCampo = false;   
    
      if($("#sltFondoPensionesAdic").val() === "")              
        validaCampo = false;       
            
      if($("#sltDepartamentoAdic").val() === "")                    
        validaCampo = false;
    
      if($("#sltDepartamentoResiAdic").val() === "")                    
        validaCampo = false;    
            
      if($("#sltMunicipioAdic").val() === "")                    
        validaCampo = false;      
    
      if($("#sltMunicipioResiAdic").val() === "")                    
        validaCampo = false;          
 
      
      var codBien = $("#sltNivelEducativoAdic").find(':selected').val();
      if(codBien === "NIVELEDUCATIVO" && $("#txtTituloProfesiAdic").val() === "")
          validaCampo = false;
      
      if($("#sltTipoViviendaAdic").val() === "")                    
        validaCampo = false;
    
            
      if($("#sltTipoAfiliadoAdic").val() === "")                    
        validaCampo = false;
  
      
      if($("#sltCargoEmpresaPrivadaAdic").val() === "")              
        validaCampo = false;
            
      if($("#sltCargoSectorPublicoAdic").val() === "")                    
        validaCampo = false;      
      
      if($("#sltModalidadContratoAdic").val() === "")                    
        validaCampo = false;
            
      if($("#sltDuracionContratoLaboralAdic").val() === "")                    
        validaCampo = false;      
      
      if($("#sltServidorPublicoAdic").val() === "")                    
        validaCampo = false;      
    
      if($("#sltSalarioRangosAdic").val() === "")                    
        validaCampo = false;          
    
      if($("#sltOrganizacionSocialAdic").val() === "")                    
        validaCampo = false;              
    
      if($("#sltCargoJuntaDirectivaAdic").val() === "")                    
        validaCampo = false;              

      if($("#sltMiembroJuntaDirectivaAdic").val() === "")                    
        validaCampo = false;          
    
      if($("#sltCondicionAfiliacionAdic").val() === "")                    
        validaCampo = false;              
       
      if($("#sltContratacionEmpleoPublicoAdic").val() === "")                    
        validaCampo = false;      
      

      if($("#txtNombreAfliliadoAdic").val() === "")              
        validaCampo = true;
              
      if($("#txtCedulaAdic").val() === "")                    
        validaCampo = true;


      if($("#sltEmpresaDondeLaboraAdic").val() === "")                    
        validaCampo = true;   


      if(validaCampo === false)
      {
        $( "#divError" ).text( "Cargando..." ).show();
        return;
      }
      else      
      {
        if(existeCedula === false)
            $( "#divError" ).text( "La Cedula ya existe, escriba una nuevo." ).show();
        else if(codBien === "NIVELEDUCATIVO" && $("#txtTituloProfesiAdic").val() === "")
        {            
            //var idx = $('#tabAdicionar a[href="#about-content"]').parent().index();
            //$("#tabAdicionar").tabs( "option", "active", idx);
            var index = $('#tabAdicionar a[href="#about-content"]').parent().index();
            $('#tabAdicionar').tabs({ active: index });
            $("#txtTituloProfesiAdic").css({'border': "1px solid #A30000"});
            $('#txtTituloProfesiAdic').focus();            
            $( "#divError" ).text( "Falta diligenciar el campo Titulo Profesional de la pesta�a informaci�n personal." ).show();
        }
        else
            $( "#divError" ).text( "Falta diligenciar campos obligatorios, revisar todas las pesta�as." ).show();
        
        event.preventDefault();        
      }
    });
});