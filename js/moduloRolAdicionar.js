$(function(){    
   
    $("form").validate({
        rules: {
          ddlModuloAdic:{
             required: true
          } 
        },
        messages: {            
            ddlModuloAdic: "Seleccione el modulo" 
        }
    });
       
    
    $("input[name=txtUsuarioAdic]").change(function(){        
        var valor = $("#txtUsuarioAdic").val();
        $("#divValUsuario").empty();
        $("#divValUsuario").load('/index.php/controladorUsuario/ValidarRol/' + valor);                
    });    
    
    $( "form" ).submit(function( event ) {
      var validaCampo = false;
      
      var existeRol = $('#divValUsuario').is(':empty');      
      if(existeRol === false)
        validaCampo = true;
       
      if(validaCampo === false)
      {
        $( "#divError" ).text( "Cargando..." ).show();
        return;
      }
      else      
      { if(existeUsuario === false)
            $( "#divError" ).text( "El modulo ya esta asignado al rol, Seleccione uno nuevo." ).show();             
         else
            $( "#divError" ).text( "Falta diligenciar campos obligatorios, revisar todas las pestañas." ).show();
        
        event.preventDefault();        
      }
    });
});