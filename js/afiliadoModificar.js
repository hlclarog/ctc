$(function(){       
    $("form").validate({
        rules: {

          txtFechaIngresoEmpresa: {
            required: false,
            date: true
          },
          txtFechaIngresoSindi: {
            required: false,
            date: true
          },
          txtCorreo:{
             required: false,
             email: true
          },
          txtNumeroHijos:{
             required: false,
             digits: true
          },

          txtNombreAfliliado:{
             required: true
          },

          txtFondoPensiones:{
             required: false
          },                    
          txtFondoCesantias:{
             required: false
          },                              
          txtArl:{
             required: false
          },                                        
          txtCajaCompensacion:{
             required: false
          },                                                  
          sltGenero: {
              required: false
          },
          sltEps: {
              required: false
          },          
          sltFondoPensiones: {
              required: false
          },          
          sltFondoCesantias: {
              required: false
          },          
          sltArl: {
              required: false
          },          
          sltCajaCompensacion: {
              required: false
          },                              
          sltDepartamento: {
              required: false
          },
          sltDepartamentoResi: {
              required: false
          },
          sltDepartamentoLabora: {
              required: false
          },          
          sltMunicipio: {
              required: false
          },
          sltMunicipioResi: {
              required: false
          },          
          sltMunicipioLabora: {
              required: false
          },                    
          sltEstadoCivil: {
              required: false
          },
          txtNumeroPreescolar: {
              required: false,
              digits: true
          },
          txtNumeroPrimaria: {
              required: false,
              digits: true
          },
          txtNumeroSecundaria: {
              required: false,
              digits: true
          },
          txtNumerotecnica: {
              required: false,
              digits: true
          },
          txtNumeroTecnologia: {
              required: false,
              digits: true
          },
          txtNumeroUniversidad: {
              required: false,
              digits: true
          },
          txtNumeropersonasCargo: {
              required: false,
              digits: true
          },
          txtNumeroHorasTrabajo: {
              required: false,
              digits: true
          },          
          txtPorcentajeCuotaSindical: {
              required: false,
              digits: true
          },                    
          txtSalarioBasico: {
              required: false,
              digits: true
          },                              
          sltNivelEducativo: {
              required: false
          },
          txtFechaRetiroSindi: {
            required: false,
            date: true
          },
          sltTipoVivienda: {
              required: false
          },
          sltTipoAfiliado: {
              required: false
          },
          sltEmpresaDondeLabora: {
              required: true
          },
          sltCargoEmpresaPrivada: {
              required: false
          },
          sltCargoSectorPublico: {
              required: false
          },
          sltModalidadContrato:{
              required: false
          },
          sltDuracionContratoLaboral: {
              required: false
          },
          sltServidorPublico: {
              required: false
          },
          sltSalarioRangos: {
              required: false
          },          
          sltOrganizacionSocial: {
              required: false
          },                    
          sltCargoJuntaDirectiva: {
              required: false
          },                    
          sltMiembroJuntaDirectiva: {
              required: false
          },                    
          sltCondicionAfiliacion: {
              required: false
          },                    
          sltContratacionEmpleoPublico:{
              required: false   
          },
          txtTelefono: { 
              minlength: 7,               
              number: true              
          },          
          txtCelular: { 
              minlength: 10,               
              number: true              
          }                              
          
        },
        messages: {            
            txtCorreo: "Escriba un email valido",
            txtFechaIngresoSindi: "Escriba o seleccione una fecha valida",
            txtFechaIngresoEmpresa: "Escriba o seleccione una fecha valida",
            txtNumeroHijos: { 
                required: "Escriba el n�mero de hijos",
                digits: "Solo se permiten n�meros"
            },

            txtNombreAfliliado: "Escriba un nombre",            
            txtFondoPensiones: "Escriba un nombre de Fondo Pensione",
            txtFondoCesantias: "Escriba un nombre de Fondo Cesantias",
            txtArl: "Escriba un nombre de Aseguradora Riesgos Laborales ",
            txtCajaCompensacion: "Escriba un nombre de la Caja de Compensaci�n",            
            sltGenero: "Seleccione un genero",
            sltEps: "Seleccione la EPS",
            sltFondoPensiones: "Seleccione el Fondo de Pensiones",
            sltFondoCesantias: "Seleccione el Fondo de Cesantias",
            sltArl: "Seleccione la Aseguradora Riesgos Laborales",
            sltCajaCompensacion: "Seleccione la Caja de Compensacion",
            sltDepartamento: "Seleccione un departamento",
            sltDepartamentoResi: "Seleccione un departamento",
            sltDepartamentoLabora: "Seleccione un departamento",
            sltMunicipio: "Seleccione un municpio",
            sltMunicipioResi: "Seleccione un municpio",
            sltMunicipioLabora: "Seleccione un municpio",
            sltEstadoCivil: "Seleccione estado civil",
            txtNumeroPreescolar: "Solo se permiten n�meros",
            txtNumeroPrimaria: "Solo se permiten n�meros",
            txtNumeroSecundaria: "Solo se permiten n�meros",
            txtNumerotecnica: "Solo se permiten n�meros",
            txtNumeroTecnologia: "Solo se permiten n�meros",
            txtNumeroUniversidad: "Solo se permiten n�meros",
            txtNumeropersonasCargo: "Solo se permiten n�meros",            
            txtNumeroHorasTrabajo: "Solo se permiten n�meros",
            txtPorcentajeCuotaSindical: "Solo se permiten n�meros",
            txtSalarioBasico: "Solo se permiten n�meros",
            sltNivelEducativo: "Seleccione el nivel educativo",
            txtFechaRetiroSindi: "Escriba o seleccione una fecha valida",
            sltTipoVivienda: "Seleccione el tipo de vivienda",
            sltTipoAfiliado: "Seleccione el tipo de afiliado",
            sltEmpresaDondeLabora: "Seleccione la empresa donde labora",
            sltCargoEmpresaPrivada: "Seleccione el cargo en empresa privada",
            sltCargoSectorPublico: "Seleccione el cargo en sector publico",
            sltModalidadContrato: "Seleccione la modalidad de contrato",
            sltDuracionContratoLaboral: "Seleccione la duracion de contrato laboral",
            sltServidorPublico: "Seleccione el tipo de servidor publico",
            sltSalarioRangos: "Seleccione el salario basico por rango",            
            sltOrganizacionSocial: "Seleccione si pertenencia a otra organizaci�n social",            
            sltCargoJuntaDirectiva: "Seleccione el cargo en junta directiva",            
            sltCondicionAfiliacion: "Seleccione la condicion de afiliacion",            
            sltMiembroJuntaDirectiva: "Seleccione miembro junta directiva",            
            sltContratacionEmpleoPublico: "Selecciona la modalidad de contratacion de empleo publico",
            txtTelefono: { 
                minlength: "El n�mero de tel�fono introducido no es correcto.",
                number: "Solo se permiten n�meros"
            },
            txtCelular: { 
                minlength: "El n�mero de Celular introducido no es correcto.",
                number: "Solo se permiten n�meros"
            },            
        }
    });
    
    $('#txtTituloProfesi').attr('disabled', 'disabled');
    
    $("#sltNivelEducativo").change(function(event){
        var codBien = $("#sltNivelEducativo").find(':selected').val();
        if(codBien === "NIVELEDUCATIVO")
            $('#txtTituloProfesi').removeAttr('disabled');
        else
            $('#txtTituloProfesi').attr('disabled', 'disabled');
    }); 
    
    $("#sltDepartamento").change(function(event){
        var id = $("#sltDepartamento").find(':selected').val();
        $("#sltMunicipio").load('/index.php/controladorAfiliado/ObtenerMunicipiosPorDepartamento/' + id);
    }); 
    
    $("#sltDepartamentoResi").change(function(event){
        var id = $("#sltDepartamentoResi").find(':selected').val();
        $("#sltMunicipioResi").load('/index.php/controladorAfiliado/ObtenerMunicipiosPorDepartamento/' + id);
    }); 
    
    $("#sltDepartamentoLabora").change(function(event){
        var id = $("#sltDepartamentoLabora").find(':selected').val();
        $("#sltMunicipioLabora").load('/index.php/controladorAfiliado/ObtenerMunicipiosPorDepartamento/' + id);
    });     
    
   $(function() {  EstudiaSi();
   $("#rdbEstudiaActualmenteSi").click(EstudiaSi);});    
    
    function EstudiaSi() {
    if (this.checked) {
        
        $('#txtQueEstudia').removeAttr('disabled');
        
        } else {
      
        $('#txtQueEstudia').attr('disabled', 'disabled');
          }
    }           
    
    $(function() {  EstudiaNo();
   $("#rdbEstudiaActualmenteNo").click(EstudiaNo);});

    function EstudiaNo() {
    if (this.checked) {
                
        $('#txtQueEstudia').attr('disabled', 'disabled');
        
        } 
    }               
    
    $(function() {  CreditoSi();
    $("#rdbEntidadCreditoSi").click(CreditoSi);});    
    
    function CreditoSi() {
    if (this.checked) {
        
        $('#txtEntidadCredito').removeAttr('disabled');
        
        } else {
      
        $('#txtEntidadCredito').attr('disabled', 'disabled');
          }
    }           
    
    $(function() {  CreditoNo();
    $("#rdbEntidadCreditoNo").click(CreditoNo);});

    function CreditoNo() {
    if (this.checked) {
                
        $('#txtEntidadCredito').attr('disabled', 'disabled');
        
        } 
    }                   
    
    $(function() {  OtroSindicatoSi();
    $("#rdbOtroSindicatoAfiliacionSi").click(OtroSindicatoSi);});    
    
    function OtroSindicatoSi() {
    if (this.checked) {
        
        $('#txtOtroSindicatoAfiliacion').removeAttr('disabled');
        
        } else {
      
        $('#txtOtroSindicatoAfiliacion').attr('disabled', 'disabled');
          }
    }           
    
    $(function() {  OtroSindicatoNo();
    $("#rdbOtroSindicatoAfiliacionNo").click(OtroSindicatoNo);});

    function OtroSindicatoNo() {
    if (this.checked) {
                
        $('#txtOtroSindicatoAfiliacion').attr('disabled', 'disabled');
        
        } 
    }               
    
    $(function() {  JuntaDirectiva();
    $("#sltMiembroJuntaDirectiva").click(JuntaDirectiva);});
    
    function JuntaDirectiva() {
    if($("#sltMiembroJuntaDirectiva").val() === "MIEMBJUNDIRECTIVANO") {
        
        $('#sltCargoJuntaDirectiva').attr('disabled', 'disabled');        
        
    } else {
        $('#sltCargoJuntaDirectiva').removeAttr('disabled');        
          }
    }       
        
    $( "form" ).submit(function( event ) {
      var validaCampo = false;
            
      var existeCedula = $('#divCedulaVal').is(':empty');      
      if(existeCedula === false)
        validaCampo = true;
    
      if($("#txtNumeroHijos").val() === "")              
        validaCampo = false;      
    
      if($("#txtFondoPensiones").val() === "")              
        validaCampo = false;    
    
      if($("#txtFondoCesantias").val() === "")              
        validaCampo = false;        
    
      if($("#txtArl").val() === "")              
        validaCampo = false;            
    
      if($("#txtCajaCompensacion").val() === "")              
        validaCampo = false;                
          
      if($("#sltGenero").val() === "")              
        validaCampo = false;
    
      if($("#sltEps").val() === "")              
        validaCampo = false;    
    
      if($("#sltArl").val() === "")              
        validaCampo = false;        
    
      if($("#sltCajaCompensacion").val() === "")              
        validaCampo = false;        
    
      if($("#sltFondoCesantias").val() === "")              
        validaCampo = false;   
    
      if($("#sltFondoPensiones").val() === "")              
        validaCampo = false;           
            
      if($("#sltDepartamento").val() === "")                    
        validaCampo = false;
    
      if($("#sltDepartamentoResi").val() === "")                    
        validaCampo = false;    
            
      if($("#sltMunicipio").val() === "")                    
        validaCampo = false;      
    
      if($("#sltMunicipioResi").val() === "")                    
        validaCampo = false;          
      
      if($("#sltNivelEducativo").val() === "")                    
        validaCampo = false;
      
      var codBien = $("#sltNivelEducativo").find(':selected').val();
      if(codBien === "NIVELEDUCATIVO" && $("#txtTituloProfesi").val() === "")
          validaCampo = false;
      
      if($("#sltTipoVivienda").val() === "")                    
        validaCampo = false;
            
      if($("#sltTipoAfiliado").val() === "")                    
        validaCampo = false;
    
      if($("#sltCargoEmpresaPrivada").val() === "")              
        validaCampo = false;
            
      if($("#sltCargoSectorPublico").val() === "")                    
        validaCampo = false;      
      
      if($("#sltModalidadContrato").val() === "")                    
        validaCampo = false;
            
      if($("#sltDuracionContratoLaboral").val() === "")                    
        validaCampo = false;      
      
      if($("#sltServidorPublico").val() === "")                    
        validaCampo = false;      
    
      if($("#sltSalarioRangos").val() === "")                    
        validaCampo = false;          
    
      if($("#sltOrganizacionSocial").val() === "")                    
        validaCampo = false;              
    
      if($("#sltCargoJuntaDirectiva").val() === "")                    
        validaCampo = false;              

      if($("#sltMiembroJuntaDirectiva").val() === "")                    
        validaCampo = false;          
    
      if($("#sltCondicionAfiliacion").val() === "")                    
        validaCampo = false;              
       
      if($("#sltContratacionEmpleoPublico").val() === "")                    
        validaCampo = false;      

      if($("#txtNombreAfliliado").val() === "")              
        validaCampo = true;

      if($("#sltEmpresaDondeLabora").val() === "")                    
        validaCampo = true;      
      
      
      if(validaCampo === false)
      {
        $( "#divError" ).text( "Cargando..." ).show();
        return;
      }
      else      
      {
        if(existeCedula === false)
            $( "#divError" ).text( "La Cedula ya existe, escriba una nuevo." ).show();
        else if(codBien === "NIVELEDUCATIVO" && $("#txtTituloProfesi").val() === "")
        {            
            //var idx = $('#tabionar a[href="#about-content"]').parent().index();
            //$("#tabionar").tabs( "option", "active", idx);
            var index = $('#tabionar a[href="#about-content"]').parent().index();
            $('#tabionar').tabs({ active: index });
            $("#txtTituloProfesi").css({'border': "1px solid #A30000"});
            $('#txtTituloProfesi').focus();            
            $( "#divError" ).text( "Falta diligenciar el campo Titulo Profesional de la pesta�a informaci�n personal." ).show();
        }
        else
            $( "#divError" ).text( "Falta diligenciar campos obligatorios, revisar todas las pesta�as." ).show();
        
        event.preventDefault();        
      }
    });
});