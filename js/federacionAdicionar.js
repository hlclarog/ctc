$(function(){    
   
    $("form").validate({
        rules: {
          txtRutAdic:{
             required: false,
             digits: true,
             minlength: 6
          },
          txtDigitoVerificacionAdic:{
             digits: true,
             maxlength: 1
          },
          txtNitadic:{
            required:true,
            digits: true,
            minlength: 6,
            maxlength: 10
          },
          txtRegistroSindicalAdic:{
            required:true,
          }, 

          txtNumeroResolucionAdic:{
             required: false,
             digits: true
          },            
          txtFechaAdic: {
            required: false,
            date: true
          },
          txtNombreFederacionAdic:{
             required: true
          },
            txtSiglaAdic: {
                required: true
            },
          sltDepartamentoAdic: {
              required: true,
          },
          sltMunicipioAdic: {
              required: true,
          },
          txtCorreoAdic:{
             required: false,
             email: true
          },                    
          sltTipoFederacionAdic: {
              required: false,
          },          
          txtFechaUltInscrJunDirectivaAdic: {
            required: false,
            date: true
          },
          sltPeriodoVigJuntaDirectivaAdic: {
              required: false,
          },

          txtFechaUltActualizacionInfAdic: {
            required: false,
            date: true
          }
        },
        messages: {            
            txtRutAdic: { 
                required: "Escriba el RUT",
                digits: "Solo se permiten nùmeros",
                minlength: "Mìnimo 6 digitos"
            },
            txtDigitoVerificacionAdic:{
                digits: "Solo se permiten nùmeros",
                maxlength: "Maximo 1 digito"
            },
            txtNitadic:{
                required: "Escriba el Nit",
                digits: "Solo se permiten nùmeros",
                minlength: "Mìnimo 6 digitos",
                maxlength: "Maximo 10 digitos"
            },
            txtRegistroSindicalAdic:{
              required: "Esciba el Registro Sindical"
            },
            txtNumeroResolucionAdic: { 
                required: "Escriba un nùmero de resoluciòn",
                digits: "Solo se permiten nùmeros"
            },                        
            txtFechaAdic: "Escriba o seleccione una fecha valida",            
            txtNombreFederacionAdic: "Escriba un nombre",
            sltDepartamentoAdic: "Seleccione un departamento",
            sltMunicipioAdic: "Seleccione un municpio",
            txtCorreoAdic: "Escriba un email valido",
            sltTipoFederacionAdic: "Seleccione un tipo",            
            txtFechaUltInscrJunDirectivaAdic: "Escriba o seleccione una fecha valida",                                               
            sltPeriodoVigJuntaDirectivaAdic: "Seleccione un periodo de vigencia",            
            txtFechaUltActualizacionInfAdic: "Escriba o seleccione una fecha valida",
            txtSiglaAdic : "Escriba sigla"
            
        }
    });

    $("input[name=txtCedulaDirectivoAdic]").change(function(){
        var valor = $("#txtCedulaDirectivoAdic").val();
        $("#tableform").empty();
        $("#tableform").load('/index.php/controladorDirectivoFederacion/ConsultarDirectivoidentification/' + valor);
    });
     
    $("input[name=txtNitAdic]").change(function(){        
        var valor = $("#txtNitAdic").val();
        $("#divNitVal").empty();
        $("#divNitVal").load('/index.php/controladorFederacion/ValidarNit/' + valor);                
    });
    
    $("#sltDepartamentoAdic").change(function(event){
        var id = $("#sltDepartamentoAdic").find(':selected').val();
        $("#sltMunicipioAdic").load('/index.php/controladorSindicato/ObtenerMunicipiosPorDepartamento/' + id);
    }); 
    
    $(function() {  Otras_secretaria();
   $("#chkOtraSecretariaAdic").click(Otras_secretaria);});

    function Otras_secretaria() {
    if (this.checked) {
        
        $('#txtOtraSecretariaAdic').removeAttr('disabled');
        
    } else {
      
        $('#txtOtraSecretariaAdic').attr('disabled', 'disabled');
          }
    }
        
    $('#txtOtrosBienesInmueblesAdic').attr('disabled', 'disabled');
    
    $('#txtOtraAfiliacionInternacionalAdic').attr('disabled', 'disabled');                    
    
    $("#sltAfiliacionIntAdic").change(function(event){
        var codTipoViolacion = $("#sltAfiliacionIntAdic").find(':selected').val();
        if(codTipoViolacion === "OTRAAFILFEDERINT")
            $('#txtOtraAfiliacionInternacionalAdic').removeAttr('disabled');
        else
            $('#txtOtraAfiliacionInternacionalAdic').attr('disabled', 'disabled');
        });                             
    
    
    $( "form" ).submit(function( event ) {
      var validaCampo = false;

        var existeCedula = $('#divCedulaVal').is(':empty');
        if(existeCedula === false)
            validaCampo = true;
      
      var existeNit = $('#divNitVal').is(':empty');      
      if(existeNit === false)
        validaCampo = true;
    
      if($("#txtNitAdic").val() === "")                    
        validaCampo = true;
    
      if($("#txtNumeroResolucionAdic").val() === "")              
        validaCampo = true;      
      
      if($("#txtNombreFederacionAdic").val() === "")              
        validaCampo = true;
              
      if($("#sltDepartamentoAdic").val() === "")                    
        validaCampo = true;
            
      if($("#sltMunicipioAdic").val() === "")                    
        validaCampo = true;      
      
      if($("#sltTipoFederacionAdic").val() === "")              
        validaCampo = true;
    
      if($("#sltPeriodoVigJuntaDirectivaAdic").val() === "")                    
        validaCampo = true;

      
      if(validaCampo === false)
      {
        $( "#divError" ).text( "Cargando..." ).show();
        return;
      }
      else      
      {
          if(existeRut === false)
            $( "#divError" ).text( "El NIT ya existe, escriba uno nuevo." ).show();
        else if(codBien === "OTROSBIENESINMUEBLES" && $("#txtOtrosBienesInmueblesAdic").val() === "")
        {            
            //var idx = $('#tabAdicionar a[href="#about-content"]').parent().index();
            //$("#tabAdicionar").tabs( "option", "active", idx);
            var index = $('#tabAdicionar a[href="#about-content"]').parent().index();
            $('#tabAdicionar').tabs({ active: index });
            $("#txtOtrosBienesInmueblesAdic").css({'border': "1px solid #A30000"});
            $('#txtOtrosBienesInmueblesAdic').focus();            
            $( "#divError" ).text( "Falta diligenciar el campo Otros Bienes Inmuebles de la pesta�a informaci�n administrativa." ).show();
        }
        else
            $( "#divError" ).text( "Falta diligenciar campos obligatorios, revisar todas las pesta�as." ).show();
        
        event.preventDefault();        
      }
    });
});