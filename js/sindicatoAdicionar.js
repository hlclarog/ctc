$(function(){       
    $("form").validate({
        rules: {
          txtFechaAdic: {
            required: false,
            date: true
          },
          txtFechaUltInscrJunDirectivaAdic: {
            required: false,
            date: true
          },
          txtCorreoAdic:{
             required: false,
             email: true
          },
          txtNumeroResolucionAdic:{
             required: false 
          },
          txtRutAdic:{
             required: false,
             digits: true,
             minlength: 6
          },
          txtNitadicD:{
             required: true,
             digits: true,
             minlength: 6
          },
          txtRegistroSindicalAdic:{
             required: true,
             digits: true,
             minlength: 6
          },          
          sltEstadoAdic: {
              required: false
          },  
          txtDigitoVerificacionAdic:{
             required: false,
             digits: true,
             maxlength: 1
          },                                                  
          txtNombreSindicatoAdic:{
             required: true
          },          

          sltClaseDirectivaAdic: {
              required: false
          },
          
          sltDepartamentoAdic: {
              required: true
          },
          sltMunicipioAdic: {
              required: true
          },
          sltPeriodoVigJuntaDirectivaAdic: {
              required: false
          },
          txtNumeroTotalAfiliadosAdic: {
              required: false,
              digits: true
          },
          txtDescripcionAfiliadosEmpresaAdic: {
              required: false,
              digits: false
          },
          txtNumeroAfiliadosHombresAdic: {
              required: false,
              digits: true
          },
          txtNumeroAfiliadosMujeresAdic: {
              required: false,
              digits: true
          },
          txtNumeroAfiliadosJovenesMenor35Adic: {
              required: false,
              digits: true
          },

          txtNumeroAfiliadosSectorFormalAdic: {
              required: false,
              digits: true
          },
          txtNumeroAfiliadosSectorInformalAdic: {
              required: false,
              digits: true
          },
          sltBienesInmueblesPropAdic: {
              required: false
          },
          txtFechaUltActualizacionInfAdic: {
            required: false,
            date: true
          },

          sltClaseSindicatoAdic: {
              required: true
          },
          sltSindicatoSegOriCapAdic: {
              required: false
          },
          sltSindicatoSegTipEmprEstAdic: {
              required: false
          },
          sltSindicatoEstModaContraAdic: {
              required: false
          },
          sltClasificacionEconSindAdic: {
              required: false
          },
          sltAfiliacionFederacionRegionalSecAdic:{
              required: true
          },
          sltFederacionAfiliacionAdic: {
              required: false
          },          
          sltAfiliacionFedRamaAdic: {
              required: false
          },
          sltCentralSindProvAdic: {
              required: false
          },
          sltAfiliacionIntAdic:{
              required: false   
          },
           txtSiglaAdic:{
              required: true   
          },
          txtTelefonoAdic: { 
              minlength: 7,               
              number: true              
          },          
          txtCelularAdic: { 
              minlength: 10,               
              number: true              
          },          
          txtFaxAdic: { 
              minlength: 7,               
              number: true              
          }          
          
        },
        messages: {            
            txtCorreoAdic: "Escriba un email valido",
            txtFechaUltInscrJunDirectivaAdic: "Escriba o seleccione una fecha valida",
            txtFechaAdic: "Escriba o seleccione una fecha valida",
            txtNumeroResolucionAdic: { 
                required: "Escriba un n�mero de resoluci�n"                
            },
            txtRutAdic: { 
                required: "Escriba el RUT",
                digits: "Solo se permiten n�meros",
                minlength: "M�nimo 6 digitos"
            },
            txtNitadic: { 
                required: "Escriba el NIT",
                digits: "Solo se permiten n�meros",
                minlength: "M�nimo 6 digitos"
            },
            txtRegistroSindicalAdic: { 
                required: "Escriba el N�mero Personer�a Jur�dica o Registro Sindical",
                digits: "Solo se permiten n�meros",
                minlength: "M�nimo 6 digitos"
            },            
            txtDigitoVerificacionAdic: { 
                required: "Escriba el Digito de Verificaci�n ",
                digits: "Solo se permiten n�meros",
                maxlength: "Maximo 1 digitos"
            },                                    
            txtNombreSindicatoAdic: "Escriba un nombre",
            txtSiglaAdic: "Escriba la sigla",
            sltClaseDirectivaAdic: "Seleccione una clase",
            sltDepartamentoAdic: "Seleccione un departamento",
            sltEstadoAdic: "Seleccione un estado",     
            sltMunicipioAdic: "Seleccione un municpio",
            sltPeriodoVigJuntaDirectivaAdic: "Seleccione un periodo de vigencia",
            txtNumeroTotalAfiliadosAdic: "Solo se permiten n�meros",
            txtDescripcionAfiliadosEmpresaAdic: "Solo se permiten n�meros",
            txtNumeroAfiliadosHombresAdic: "Solo se permiten n�meros",
            txtNumeroAfiliadosMujeresAdic: "Solo se permiten n�meros",            
            txtNumeroAfiliadosJovenesMenor35Adic: "Solo se permiten n�meros",
            txtNumeroAfiliadosSectorFormalAdic: "Solo se permiten n�meros",
            txtNumeroAfiliadosSectorInformalAdic: "Solo se permiten n�meros",            
            sltBienesInmueblesPropAdic: "Seleccione un bien inmueble",
            txtFechaUltActualizacionInfAdic: "Escriba o seleccione una fecha valida",            
            sltClaseSindicatoAdic: "Seleccione una clase de sindicato",
            sltSindicatoSegOriCapAdic: "Seleccione un sindicato seg�n origen",
            sltSindicatoSegTipEmprEstAdic: "Seleccione un sindicato seg�n tipo de empresa",
            sltSindicatoEstModaContraAdic: "Seleccione un sindicato estatal seg�n modalidad",
            sltClasificacionEconSindAdic: "Seleccione una clasificaci�n econ�mica",
            sltAfiliacionFederacionRegionalSecAdic: "Seleccione afiliaci�n regional",
            sltFederacionAfiliacionAdic: "Seleccione la federaci�n de afiliaci�n ",
            sltAfiliacionFedRamaAdic: "Seleccione una afiliaci�n a federaci�n",
            sltCentralSindProvAdic: "Seleccione una central",
            sltAfiliacionIntAdic: "Selecciona una afiliaci�n",
            txtTelefonoAdic : { 
                minlength: "El n�mero de tel�fono introducido no es correcto.",
                number: "Solo se permiten n�meros"
            },
            txtCelularAdic : { 
                minlength: "El n�mero de Celular introducido no es correcto.",
                number: "Solo se permiten n�meros"
            },
            txtFaxAdic : { 
                minlength: "El n�mero de Fax introducido no es correcto.",
                number: "Solo se permiten n�meros"
            }                   
        }
    });
    
   $(function() {  OtrosBienesInmuebles();
   $("#chkOtrosBienesInmueblesAdic").click(OtrosBienesInmuebles);});

    function OtrosBienesInmuebles() {
    if (this.checked) {
        
        $('#txtOtrosBienesInmueblesAdic').removeAttr('disabled');
        
    } else {
      
        $('#txtOtrosBienesInmueblesAdic').attr('disabled', 'disabled');
          }
    }           
    
   $(function() {  Otras_secretaria();
   $("#chkOtraSecretariaAdic").click(Otras_secretaria);});

    function Otras_secretaria() {
    if (this.checked) {
        
        $('#txtOtraSecretariaAdic').removeAttr('disabled');
        
    } else {
      
        $('#txtOtraSecretariaAdic').attr('disabled', 'disabled');
          }
    }   
    
    $(function() {  tipoViolenciaSi();
    $("#rdbVictimaViolenciaSiAdic").click(tipoViolenciaSi);});

    function tipoViolenciaSi() {
    if (this.checked) {        
        
        $('#chkAllanamientoIlegalAdic').removeAttr('disabled');
        $('#chkAmenazasAdic').removeAttr('disabled');
        $('#chkAtentadoLesionesAdic').removeAttr('disabled');
        $('#chkDesaparicionAdic').removeAttr('disabled');
        $('#chkDesplazamientoForzosoAdic').removeAttr('disabled');
        $('#chkDetencionArbitrariaAdic').removeAttr('disabled');
        $('#chkHomicidiosAdic').removeAttr('disabled');
        $('#chkHostigamientoAdic').removeAttr('disabled');
        $('#chkSecuestroAdic').removeAttr('disabled');
        $('#chkOtroTipoViolenciaAdic').removeAttr('disabled');        
        
        
        } else {
                                   
            $('#chkAllanamientoIlegalAdic').attr('disabled', 'disabled');
            $('#chkAmenazasAdic').attr('disabled', 'disabled');
            $('#chkAtentadoLesionesAdic').attr('disabled', 'disabled');
            $('#chkDesaparicionAdic').attr('disabled', 'disabled');
            $('#chkDesplazamientoForzosoAdic').attr('disabled', 'disabled');
            $('#chkDetencionArbitrariaAdic').attr('disabled', 'disabled');
            $('#chkHomicidiosAdic').attr('disabled', 'disabled');
            $('#chkHostigamientoAdic').attr('disabled', 'disabled');
            $('#chkSecuestroAdic').attr('disabled', 'disabled');
            $('#chkOtroTipoViolenciaAdic').attr('disabled', 'disabled');
        
          }
    }               
    
    $(function() {  tipoViolenciaNo();
    $("#rdbVictimaViolenciaNoAdic").click(tipoViolenciaNo);});

    function tipoViolenciaNo() {
    if (this.checked) {
                
        $('#chkAllanamientoIlegalAdic').attr('disabled', 'disabled');
            $('#chkAmenazasAdic').attr('disabled', 'disabled');
            $('#chkAtentadoLesionesAdic').attr('disabled', 'disabled');
            $('#chkDesaparicionAdic').attr('disabled', 'disabled');
            $('#chkDesplazamientoForzosoAdic').attr('disabled', 'disabled');
            $('#chkDetencionArbitrariaAdic').attr('disabled', 'disabled');
            $('#chkHomicidiosAdic').attr('disabled', 'disabled');
            $('#chkHostigamientoAdic').attr('disabled', 'disabled');
            $('#chkSecuestroAdic').attr('disabled', 'disabled');
            $('#chkOtroTipoViolenciaAdic').attr('disabled', 'disabled');
        } 
    }               
    
    $(function() {  OtroTipoViolencia();
    $("#chkOtroTipoViolenciaAdic").click(OtroTipoViolencia);});

    function OtroTipoViolencia() {
    if (this.checked || $("#chkOtroTipoViolenciaAdic").attr('checked')  ) {
      $('#txtOtroTipoViolenciaAdic').removeAttr('disabled');         
        
    } else {
      
      $('#txtOtroTipoViolenciaAdic').attr('disabled', 'disabled');                
          }
    }            
    
    $(function() {  Estado();
    $("#sltEstadoAdic").click(Estado);});
    
    function Estado() {
    if($("#sltEstadoAdic").val() === "INACTIVO") {
        
        $('#sltCaracteristicasSindicatoInactivoAdic').removeAttr('disabled');
        
    } else {
      
        $('#sltCaracteristicasSindicatoInactivoAdic').attr('disabled', 'disabled');
          }
    }   
    
    $(function() {  Fusionado();
    $("#sltCaracteristicasSindicatoInactivoAdic").click(Fusionado);});
    
    function Fusionado() {
    if($("#sltCaracteristicasSindicatoInactivoAdic").val() === "SINDICATOFUSIONADO") {
        
        $('#txtNombreSindicatoFusionaAdic').removeAttr('disabled');
        
    } else {
      
        $('#txtNombreSindicatoFusionaAdic').attr('disabled', 'disabled');
          }
    }           
    
    $("#sltDepartamentoAdic").change(function(event){
        var id = $("#sltDepartamentoAdic").find(':selected').val();
        $("#sltMunicipioAdic").load('/index.php/controladorSindicato/ObtenerMunicipiosPorDepartamento/' + id);
    }); 
        
    $("input[name=txtRutAdic]").change(function(){        
        var valor = $("#txtRutAdic").val();
        $("#divRutVal").empty();
        $("#divRutVal").load('/index.php/controladorSindicato/ValidarRut/' + valor);                
    });
    
    $("input[name=txtRegistroSindicalAdic]").change(function(){        
        var valor = $("#txtRegistroSindicalAdic").val();
        $("#divRegistroSindicalVal").empty();
        $("#divRegistroSindicalVal").load('/index.php/controladorSindicato/ValidardivRegistroSindicalVal/' + valor);                
    });    
        
    $( "form" ).submit(function( event ) {
      var validaCampo = false;
            
      var existeRut = $('#divRutVal').is(':empty');      
      if(existeRut === false)
        validaCampo = true;
    
      if($("#txtNumeroResolucionAdic").val() === "")              
        validaCampo = false;      
              
      if($("#txtRutAdic").val() === "")                    
        validaCampo = false;    
   
      if($("#sltClaseDirectivaAdic").val() === "")              
        validaCampo = false;    

      if($("#txtDigitoVerificacionAdic").val() === "")                    
        validaCampo = false;    
    
      if($("#sltEstadoAdic").val() === "")                   
        validaCampo = false;  

      if($("#sltBienesInmueblesPropAdic").val() === "")                    
        validaCampo = false;       
      
      var codBien = $("#sltBienesInmueblesPropAdic").find(':selected').val();
      if(codBien === "OTROSBIENESINMUEBLES" && $("#txtOtrosBienesInmueblesAdic").val() === "")
          validaCampo = false;
      
      if($("#sltClaseSindicatoAdic").val() === "")                    
        validaCampo = false;
            
      if($("#sltSindicatoSegOriCapAdic").val() === "")                    
        validaCampo = false;
            
      if($("#sltSindicatoSegTipEmprEstAdic").val() === "")                    
        validaCampo = false;      
      
      if($("#sltSindicatoEstModaContraAdic").val() === "")              
        validaCampo = false;
            
      if($("#sltClasificacionEconSindAdic").val() === "")                    
        validaCampo = false;      
      
      if($("#sltAfiliacionFederacionRegionalSecAdic").val() === "")                    
        validaCampo = false;
            
      if($("#sltFederacionAfiliacionAdic").val() === "")                    
        validaCampo = false;      
    
      if($("#sltAfiliacionFedRamaAdic").val() === "")                    
        validaCampo = false;       
      
      if($("#sltCentralSindProvAdic").val() === "")                    
        validaCampo = false;      
       
      if($("#sltAfiliacionIntAdic").val() === "")                    
        validaCampo = false;   



      if($("#sltDepartamentoAdic").val() === "")                    
        validaCampo = true;
            
      if($("#sltMunicipioAdic").val() === "")                    
        validaCampo = true;      

       if($("#txtNombreSindicatoAdic").val() === "")              
        validaCampo = true;

      if($("#txtRegistroSindicalAdic").val() === "")                    
        validaCampo = true;      
      
      if(validaCampo === false)
      {
        $( "#divError" ).text( "Cargando..." ).show();
        return;
      }
      else      
      {
        if(existeRut === false)
            $( "#divError" ).text( "El RUT ya existe, escriba uno nuevo." ).show();
        else if(codBien === "OTROSBIENESINMUEBLES" && $("#txtOtrosBienesInmueblesAdic").val() === "")
        {            
            //var idx = $('#tabAdicionar a[href="#about-content"]').parent().index();
            //$("#tabAdicionar").tabs( "option", "active", idx);
            var index = $('#tabAdicionar a[href="#about-content"]').parent().index();
            $('#tabAdicionar').tabs({ active: index });
            $("#txtOtrosBienesInmueblesAdic").css({'border': "1px solid #A30000"});
            $('#txtOtrosBienesInmueblesAdic').focus();            
            $( "#divError" ).text( "Falta diligenciar el campo Otros Bienes Inmuebles de la pesta�a informaci�n administrativa." ).show();
        }
        else
            $( "#divError" ).text( "Falta diligenciar campos obligatorios, revisar todas las pesta�as." ).show();
        
        event.preventDefault();        
      }
    });
});