<div id="tabConsultar" align="center" style="padding-top: 25px;">
    <table id="table" class="display" cellspacing="0" width="100%">
        <thead class="trTitulo">
        <tr>
            <th></th>
            <th>Nombre Usuario</th>
            <th>Nombre Tabla</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>

        <?php
        /*
         * * $registros: Array en donde se obtienen los resultados del
         * * $registro: Donde se almacenaran el registro actual para graficar
         */
        if($tituloTab != 'Consulta')
        {
            if(!isset($consultar))
            {
                foreach($registros as $registro)
                {
                    echo "<tr>";
                    if($usuario['perfil'] == "Lector")
                        echo '<td>
                                                <a href="/index.php/controladorDepartamento/ConsultarDepartamento/'.$registro['codigo'].'/1" title="Consultar">
                                                    <img src="/images/buscar.png" width="15" height="15" alt="Consultar"/>
                                                </a>
                                            </td>';
                    else
                    {
                        echo '<td>
                                                <a href="/index.php/controladorDepartamento/ConsultarDepartamento/'.$registro['codigo'].'" title="Modificar">
                                                    <img src="/images/editar.jpg" width="15" height="15"  alt="Editar"/>
                                                </a>                                                                          
                                                <a href="javascript:;" onclick="Confirmar(\'/index.php/controladorDepartamento/EliminarDepartamento/'.$registro['codigo'].'\'); return false;"  title="Eliminar">
                                                    <img src="/images/eliminar.png" width="15" height="15" alt="Eliminar"/>
                                                </a>
                                            </td>';
                    }
                    echo "<td>".utf8_decode($registro['codigo'])."</td>";
                    echo "<td>".utf8_decode($registro['descripcion'])."</th>";
                    echo "<td>"."</td>";
                    echo "<td>"."</td>";
                    echo "</tr>";
                }

            }
            else
                $registro = $registros[0];
        }
        else
            $registro = $registros[0];
        ?>

        </tbody>
    </table>
    <div>
        <div id="divExportarExcel">
            <a href="/index.php/controladorDepartamento/GenerarExcel" target="_blank" title="Exportar a formato Excel">
                <img src="/images/excel.jpg" width="30" height="30" />
                <br />
                Exportar a Excel
            </a>
        </div>
        <div id="divExportarPdf">
            <a href="/index.php/controladorDepartamento/GenerarPdf" target="_blank" title="Exportar a formato PDF">
                <img src="/images/pdf.jpg" width="30" height="30" />
                <br />
                Exportar a Pdf
            </a>
        </div>
        <div class="clearBoth"></div>
    </div>
</div>